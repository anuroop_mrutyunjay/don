#!/bin/sh
./tests/tensorflow/test_conv1d.sh
./tests/tensorflow/test_conv1d_1.sh
./tests/tensorflow/test_conv1d_conv1d.sh
./tests/tensorflow/test_conv1d_conv1d_dense.sh
./tests/tensorflow/test_conv1d_conv2d.sh
./tests/tensorflow/test_conv1d_conv2d_dense.sh
./tests/tensorflow/test_conv1d_relu_avgpool.sh
./tests/tensorflow/test_conv1d_relu_avgpool_dense.sh
./tests/tensorflow/test_conv1d_relu_bias.sh
./tests/tensorflow/test_conv1d_relu_maxpool.sh
./tests/tensorflow/test_conv1d_relu_maxpool_dense.sh
./tests/tensorflow/test_conv2d.sh
./tests/tensorflow/test_conv2dTranspose.sh
./tests/tensorflow/test_conv2d_conv2d_dense.sh
./tests/tensorflow/test_conv2d_relu.sh
./tests/tensorflow/test_conv2d_relu__avgpool__dense.sh
./tests/tensorflow/test_conv2d_relu__maxpool__dense.sh
./tests/tensorflow/test_conv2d_relu_avgpool_bias.sh
./tests/tensorflow/test_conv2d_relu_bias.sh
./tests/tensorflow/test_conv2d_relu_maxpool_bias.sh
./tests/tensorflow/test_dense.sh
./tests/tensorflow/test_simpleNet1.sh
./tests/tensorflow/test_simpleNet2.sh
./tests/tensorflow/test_simpleNet3.sh
#./tests/tensorflow/test_tr_conv2d_tr_dense.sh


