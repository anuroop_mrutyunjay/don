Іи
ћҐ
B
AssignVariableOp
resource
value"dtype"
dtypetypeИ
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype
Ы
Conv2D

input"T
filter"T
output"T"
Ttype:	
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
:
Maximum
x"T
y"T
z"T"
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(И
:
Minimum
x"T
y"T
z"T"
Ttype:

2	

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
_
Pad

input"T
paddings"	Tpaddings
output"T"	
Ttype"
	Tpaddingstype0:
2	
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetypeИ
E
Relu
features"T
activations"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0И
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0И
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
Њ
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring И
@
StaticRegexFullMatch	
input

output
"
patternstring
ц
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
Ц
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 И"serve*2.4.12v2.4.1-0-g85c8b2a817f8иж
§
 fused_conv2d_re_lu/conv2d/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*1
shared_name" fused_conv2d_re_lu/conv2d/kernel
Э
4fused_conv2d_re_lu/conv2d/kernel/Read/ReadVariableOpReadVariableOp fused_conv2d_re_lu/conv2d/kernel*&
_output_shapes
:*
dtype0
М
fused_dense/dense/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
: *)
shared_namefused_dense/dense/kernel
Е
,fused_dense/dense/kernel/Read/ReadVariableOpReadVariableOpfused_dense/dense/kernel*
_output_shapes

: *
dtype0
Д
fused_dense/dense/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*'
shared_namefused_dense/dense/bias
}
*fused_dense/dense/bias/Read/ReadVariableOpReadVariableOpfused_dense/dense/bias*
_output_shapes
:*
dtype0

NoOpNoOp
Ѓ
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*й
valueяB№ B’
ж
layer-0
layer-1
layer_with_weights-0
layer-2
layer-3
layer_with_weights-1
layer-4
	optimizer
trainable_variables
	variables
	regularization_losses

	keras_api

signatures
 
R
trainable_variables
	variables
regularization_losses
	keras_api
Ђ
zeropadding

conv2d
quantize_pool

clamp_pool
quantize
	clamp
trainable_variables
	variables
regularization_losses
	keras_api
R
trainable_variables
	variables
regularization_losses
	keras_api
v
	Dense
quantize
	 clamp
!trainable_variables
"	variables
#regularization_losses
$	keras_api
 

%0
&1
'2

%0
&1
'2
 
≠
(non_trainable_variables
)layer_metrics
trainable_variables
	variables

*layers
+metrics
	regularization_losses
,layer_regularization_losses
 
 
 
 
≠
-layer_metrics
.non_trainable_variables
trainable_variables
	variables

/layers
0metrics
regularization_losses
1layer_regularization_losses
R
2trainable_variables
3	variables
4regularization_losses
5	keras_api
^

%kernel
6trainable_variables
7	variables
8regularization_losses
9	keras_api

:	keras_api

;	keras_api

<	keras_api
R
=trainable_variables
>	variables
?regularization_losses
@	keras_api

%0

%0
 
≠
Alayer_metrics
Bnon_trainable_variables
trainable_variables
	variables

Clayers
Dmetrics
regularization_losses
Elayer_regularization_losses
 
 
 
≠
Flayer_metrics
Gnon_trainable_variables
trainable_variables
	variables

Hlayers
Imetrics
regularization_losses
Jlayer_regularization_losses
h

&kernel
'bias
Ktrainable_variables
L	variables
Mregularization_losses
N	keras_api

O	keras_api
R
Ptrainable_variables
Q	variables
Rregularization_losses
S	keras_api

&0
'1

&0
'1
 
≠
Tlayer_metrics
Unon_trainable_variables
!trainable_variables
"	variables

Vlayers
Wmetrics
#regularization_losses
Xlayer_regularization_losses
fd
VARIABLE_VALUE fused_conv2d_re_lu/conv2d/kernel0trainable_variables/0/.ATTRIBUTES/VARIABLE_VALUE
^\
VARIABLE_VALUEfused_dense/dense/kernel0trainable_variables/1/.ATTRIBUTES/VARIABLE_VALUE
\Z
VARIABLE_VALUEfused_dense/dense/bias0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUE
 
 
#
0
1
2
3
4
 
 
 
 
 
 
 
 
 
 
≠
Ylayer_metrics
Znon_trainable_variables
2trainable_variables
3	variables

[layers
\metrics
4regularization_losses
]layer_regularization_losses

%0

%0
 
≠
^layer_metrics
_non_trainable_variables
6trainable_variables
7	variables

`layers
ametrics
8regularization_losses
blayer_regularization_losses
 
 
 
 
 
 
≠
clayer_metrics
dnon_trainable_variables
=trainable_variables
>	variables

elayers
fmetrics
?regularization_losses
glayer_regularization_losses
 
 
*
0
1
2
3
4
5
 
 
 
 
 
 
 

&0
'1

&0
'1
 
≠
hlayer_metrics
inon_trainable_variables
Ktrainable_variables
L	variables

jlayers
kmetrics
Mregularization_losses
llayer_regularization_losses
 
 
 
 
≠
mlayer_metrics
nnon_trainable_variables
Ptrainable_variables
Q	variables

olayers
pmetrics
Rregularization_losses
qlayer_regularization_losses
 
 

0
1
 2
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
В
serving_default_input_1Placeholder*+
_output_shapes
:€€€€€€€€€*
dtype0* 
shape:€€€€€€€€€
Љ
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1 fused_conv2d_re_lu/conv2d/kernelfused_dense/dense/kernelfused_dense/dense/bias*
Tin
2*
Tout
2*
_collective_manager_ids
 *U
_output_shapesC
A:€€€€€€€€€ :€€€€€€€€€:€€€€€€€€€*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В **
f%R#
!__inference_signature_wrapper_703
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
Ѓ
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename4fused_conv2d_re_lu/conv2d/kernel/Read/ReadVariableOp,fused_dense/dense/kernel/Read/ReadVariableOp*fused_dense/dense/bias/Read/ReadVariableOpConst*
Tin	
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *%
f R
__inference__traced_save_961
н
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filename fused_conv2d_re_lu/conv2d/kernelfused_dense/dense/kernelfused_dense/dense/bias*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *(
f#R!
__inference__traced_restore_980ДЈ
ґ
\
@__inference_flatten_layer_call_and_return_conditional_losses_876

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"€€€€    2
Constg
ReshapeReshapeinputsConst:output:0*
T0*'
_output_shapes
:€€€€€€€€€ 2	
Reshaped
IdentityIdentityReshape:output:0*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€:W S
/
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs
«

®
#__inference_model_layer_call_fn_655
input_1
unknown
	unknown_0
	unknown_1
identity

identity_1

identity_2ИҐStatefulPartitionedCallѓ
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *U
_output_shapesC
A:€€€€€€€€€:€€€€€€€€€ :€€€€€€€€€*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *G
fBR@
>__inference_model_layer_call_and_return_conditional_losses_6422
StatefulPartitionedCallЦ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

IdentityТ

Identity_1Identity StatefulPartitionedCall:output:1^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity_1Т

Identity_2Identity StatefulPartitionedCall:output:2^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::22
StatefulPartitionedCallStatefulPartitionedCall:T P
+
_output_shapes
:€€€€€€€€€
!
_user_specified_name	input_1
÷
ѓ
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_856
x)
%conv2d_conv2d_readvariableop_resource
identityИҐconv2d/Conv2D/ReadVariableOpЂ
zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d/Pad/paddingsТ
zero_padding2d/PadPadx$zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
zero_padding2d/Pad™
conv2d/Conv2D/ReadVariableOpReadVariableOp%conv2d_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
conv2d/Conv2D/ReadVariableOpќ
conv2d/Conv2DConv2Dzero_padding2d/Pad:output:0$conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€*
paddingVALID*
strides
2
conv2d/Conv2Dt
conv2d/ReluReluconv2d/Conv2D:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
conv2d/ReluГ
clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  А?2
clamp/clip_by_value/Minimum/y¬
clamp/clip_by_value/MinimumMinimumconv2d/Relu:activations:0&clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
clamp/clip_by_value/Minimums
clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  Ањ2
clamp/clip_by_value/y∞
clamp/clip_by_valueMaximumclamp/clip_by_value/Minimum:z:0clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
clamp/clip_by_valueТ
IdentityIdentityclamp/clip_by_value:z:0^conv2d/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€:2<
conv2d/Conv2D/ReadVariableOpconv2d/Conv2D/ReadVariableOp:R N
/
_output_shapes
:€€€€€€€€€

_user_specified_namex
†
Ц
>__inference_model_layer_call_and_return_conditional_losses_673

inputs
fused_conv2d_re_lu_661
fused_dense_665
fused_dense_667
identity

identity_1

identity_2ИҐ*fused_conv2d_re_lu/StatefulPartitionedCallҐ#fused_dense/StatefulPartitionedCallў
reshape/PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:€€€€€€€€€* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *I
fDRB
@__inference_reshape_layer_call_and_return_conditional_losses_4822
reshape/PartitionedCall»
*fused_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall reshape/PartitionedCall:output:0fused_conv2d_re_lu_661*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:€€€€€€€€€*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *T
fORM
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_5182,
*fused_conv2d_re_lu/StatefulPartitionedCallю
flatten/PartitionedCallPartitionedCall3fused_conv2d_re_lu/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:€€€€€€€€€ * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *I
fDRB
@__inference_flatten_layer_call_and_return_conditional_losses_5432
flatten/PartitionedCallЈ
#fused_dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0fused_dense_665fused_dense_667*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:€€€€€€€€€*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *M
fHRF
D__inference_fused_dense_layer_call_and_return_conditional_losses_5792%
#fused_dense/StatefulPartitionedCallв
IdentityIdentity3fused_conv2d_re_lu/StatefulPartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

IdentityЋ

Identity_1Identity flatten/PartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity_1„

Identity_2Identity,fused_dense/StatefulPartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::2X
*fused_conv2d_re_lu/StatefulPartitionedCall*fused_conv2d_re_lu/StatefulPartitionedCall2J
#fused_dense/StatefulPartitionedCall#fused_dense/StatefulPartitionedCall:S O
+
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs
†
Ц
>__inference_model_layer_call_and_return_conditional_losses_642

inputs
fused_conv2d_re_lu_630
fused_dense_634
fused_dense_636
identity

identity_1

identity_2ИҐ*fused_conv2d_re_lu/StatefulPartitionedCallҐ#fused_dense/StatefulPartitionedCallў
reshape/PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:€€€€€€€€€* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *I
fDRB
@__inference_reshape_layer_call_and_return_conditional_losses_4822
reshape/PartitionedCall»
*fused_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall reshape/PartitionedCall:output:0fused_conv2d_re_lu_630*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:€€€€€€€€€*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *T
fORM
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_5042,
*fused_conv2d_re_lu/StatefulPartitionedCallю
flatten/PartitionedCallPartitionedCall3fused_conv2d_re_lu/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:€€€€€€€€€ * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *I
fDRB
@__inference_flatten_layer_call_and_return_conditional_losses_5432
flatten/PartitionedCallЈ
#fused_dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0fused_dense_634fused_dense_636*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:€€€€€€€€€*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *M
fHRF
D__inference_fused_dense_layer_call_and_return_conditional_losses_5652%
#fused_dense/StatefulPartitionedCallв
IdentityIdentity3fused_conv2d_re_lu/StatefulPartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

IdentityЋ

Identity_1Identity flatten/PartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity_1„

Identity_2Identity,fused_dense/StatefulPartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::2X
*fused_conv2d_re_lu/StatefulPartitionedCall*fused_conv2d_re_lu/StatefulPartitionedCall2J
#fused_dense/StatefulPartitionedCall#fused_dense/StatefulPartitionedCall:S O
+
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs
£7
ѓ
>__inference_model_layer_call_and_return_conditional_losses_779

inputs<
8fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resource4
0fused_dense_dense_matmul_readvariableop_resource5
1fused_dense_dense_biasadd_readvariableop_resource
identity

identity_1

identity_2ИҐ/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOpҐ(fused_dense/dense/BiasAdd/ReadVariableOpҐ'fused_dense/dense/MatMul/ReadVariableOpT
reshape/ShapeShapeinputs*
T0*
_output_shapes
:2
reshape/ShapeД
reshape/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
reshape/strided_slice/stackИ
reshape/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
reshape/strided_slice/stack_1И
reshape/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
reshape/strided_slice/stack_2Т
reshape/strided_sliceStridedSlicereshape/Shape:output:0$reshape/strided_slice/stack:output:0&reshape/strided_slice/stack_1:output:0&reshape/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
reshape/strided_slicet
reshape/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
reshape/Reshape/shape/1t
reshape/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
reshape/Reshape/shape/2t
reshape/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
reshape/Reshape/shape/3к
reshape/Reshape/shapePackreshape/strided_slice:output:0 reshape/Reshape/shape/1:output:0 reshape/Reshape/shape/2:output:0 reshape/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
reshape/Reshape/shapeП
reshape/ReshapeReshapeinputsreshape/Reshape/shape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
reshape/Reshape—
.fused_conv2d_re_lu/zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             20
.fused_conv2d_re_lu/zero_padding2d/Pad/paddingsв
%fused_conv2d_re_lu/zero_padding2d/PadPadreshape/Reshape:output:07fused_conv2d_re_lu/zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€2'
%fused_conv2d_re_lu/zero_padding2d/Padг
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOpReadVariableOp8fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype021
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOpЪ
 fused_conv2d_re_lu/conv2d/Conv2DConv2D.fused_conv2d_re_lu/zero_padding2d/Pad:output:07fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€*
paddingVALID*
strides
2"
 fused_conv2d_re_lu/conv2d/Conv2D≠
fused_conv2d_re_lu/conv2d/ReluRelu)fused_conv2d_re_lu/conv2d/Conv2D:output:0*
T0*/
_output_shapes
:€€€€€€€€€2 
fused_conv2d_re_lu/conv2d/Relu©
0fused_conv2d_re_lu/clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  А?22
0fused_conv2d_re_lu/clamp/clip_by_value/Minimum/yО
.fused_conv2d_re_lu/clamp/clip_by_value/MinimumMinimum,fused_conv2d_re_lu/conv2d/Relu:activations:09fused_conv2d_re_lu/clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€20
.fused_conv2d_re_lu/clamp/clip_by_value/MinimumЩ
(fused_conv2d_re_lu/clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  Ањ2*
(fused_conv2d_re_lu/clamp/clip_by_value/yь
&fused_conv2d_re_lu/clamp/clip_by_valueMaximum2fused_conv2d_re_lu/clamp/clip_by_value/Minimum:z:01fused_conv2d_re_lu/clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€2(
&fused_conv2d_re_lu/clamp/clip_by_valueo
flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"€€€€    2
flatten/Const£
flatten/ReshapeReshape*fused_conv2d_re_lu/clamp/clip_by_value:z:0flatten/Const:output:0*
T0*'
_output_shapes
:€€€€€€€€€ 2
flatten/Reshape√
'fused_dense/dense/MatMul/ReadVariableOpReadVariableOp0fused_dense_dense_matmul_readvariableop_resource*
_output_shapes

: *
dtype02)
'fused_dense/dense/MatMul/ReadVariableOpї
fused_dense/dense/MatMulMatMulflatten/Reshape:output:0/fused_dense/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
fused_dense/dense/MatMul¬
(fused_dense/dense/BiasAdd/ReadVariableOpReadVariableOp1fused_dense_dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02*
(fused_dense/dense/BiasAdd/ReadVariableOp…
fused_dense/dense/BiasAddBiasAdd"fused_dense/dense/MatMul:product:00fused_dense/dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
fused_dense/dense/BiasAddЯ
+fused_dense/clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2-
+fused_dense/clamp_1/clip_by_value/Minimum/yн
)fused_dense/clamp_1/clip_by_value/MinimumMinimum"fused_dense/dense/BiasAdd:output:04fused_dense/clamp_1/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2+
)fused_dense/clamp_1/clip_by_value/MinimumП
#fused_dense/clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   »2%
#fused_dense/clamp_1/clip_by_value/yа
!fused_dense/clamp_1/clip_by_valueMaximum-fused_dense/clamp_1/clip_by_value/Minimum:z:0,fused_dense/clamp_1/clip_by_value/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2#
!fused_dense/clamp_1/clip_by_valueН
IdentityIdentity*fused_conv2d_re_lu/clamp/clip_by_value:z:00^fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp)^fused_dense/dense/BiasAdd/ReadVariableOp(^fused_dense/dense/MatMul/ReadVariableOp*
T0*/
_output_shapes
:€€€€€€€€€2

Identityч

Identity_1Identityflatten/Reshape:output:00^fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp)^fused_dense/dense/BiasAdd/ReadVariableOp(^fused_dense/dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity_1Д

Identity_2Identity%fused_dense/clamp_1/clip_by_value:z:00^fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp)^fused_dense/dense/BiasAdd/ReadVariableOp(^fused_dense/dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::2b
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp2T
(fused_dense/dense/BiasAdd/ReadVariableOp(fused_dense/dense/BiasAdd/ReadVariableOp2R
'fused_dense/dense/MatMul/ReadVariableOp'fused_dense/dense/MatMul/ReadVariableOp:S O
+
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs
÷
ѓ
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_518
x)
%conv2d_conv2d_readvariableop_resource
identityИҐconv2d/Conv2D/ReadVariableOpЂ
zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d/Pad/paddingsТ
zero_padding2d/PadPadx$zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
zero_padding2d/Pad™
conv2d/Conv2D/ReadVariableOpReadVariableOp%conv2d_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
conv2d/Conv2D/ReadVariableOpќ
conv2d/Conv2DConv2Dzero_padding2d/Pad:output:0$conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€*
paddingVALID*
strides
2
conv2d/Conv2Dt
conv2d/ReluReluconv2d/Conv2D:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
conv2d/ReluГ
clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  А?2
clamp/clip_by_value/Minimum/y¬
clamp/clip_by_value/MinimumMinimumconv2d/Relu:activations:0&clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
clamp/clip_by_value/Minimums
clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  Ањ2
clamp/clip_by_value/y∞
clamp/clip_by_valueMaximumclamp/clip_by_value/Minimum:z:0clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
clamp/clip_by_valueТ
IdentityIdentityclamp/clip_by_value:z:0^conv2d/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€:2<
conv2d/Conv2D/ReadVariableOpconv2d/Conv2D/ReadVariableOp:R N
/
_output_shapes
:€€€€€€€€€

_user_specified_namex
й
\
@__inference_reshape_layer_call_and_return_conditional_losses_823

inputs
identityD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2в
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliced
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3Ї
Reshape/shapePackstrided_slice:output:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shapew
ReshapeReshapeinputsReshape/shape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2	
Reshapel
IdentityIdentityReshape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0**
_input_shapes
:€€€€€€€€€:S O
+
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs
Н
р
D__inference_fused_dense_layer_call_and_return_conditional_losses_895
x(
$dense_matmul_readvariableop_resource)
%dense_biasadd_readvariableop_resource
identityИҐdense/BiasAdd/ReadVariableOpҐdense/MatMul/ReadVariableOpЯ
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

: *
dtype02
dense/MatMul/ReadVariableOpА
dense/MatMulMatMulx#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
dense/MatMulЮ
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
dense/BiasAdd/ReadVariableOpЩ
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
dense/BiasAddЗ
clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2!
clamp_1/clip_by_value/Minimum/yљ
clamp_1/clip_by_value/MinimumMinimumdense/BiasAdd:output:0(clamp_1/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2
clamp_1/clip_by_value/Minimumw
clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   »2
clamp_1/clip_by_value/y∞
clamp_1/clip_by_valueMaximum!clamp_1/clip_by_value/Minimum:z:0 clamp_1/clip_by_value/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2
clamp_1/clip_by_value™
IdentityIdentityclamp_1/clip_by_value:z:0^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€ ::2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp:J F
'
_output_shapes
:€€€€€€€€€ 

_user_specified_namex
б
c
G__inference_zero_padding2d_layer_call_and_return_conditional_losses_458

inputs
identityН
Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
Pad/paddingsЕ
PadPadinputsPad/paddings:output:0*
T0*J
_output_shapes8
6:4€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2
PadГ
IdentityIdentityPad:output:0*
T0*J
_output_shapes8
6:4€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€:r n
J
_output_shapes8
6:4€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€
 
_user_specified_nameinputs
£
Ч
>__inference_model_layer_call_and_return_conditional_losses_623
input_1
fused_conv2d_re_lu_611
fused_dense_615
fused_dense_617
identity

identity_1

identity_2ИҐ*fused_conv2d_re_lu/StatefulPartitionedCallҐ#fused_dense/StatefulPartitionedCallЏ
reshape/PartitionedCallPartitionedCallinput_1*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:€€€€€€€€€* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *I
fDRB
@__inference_reshape_layer_call_and_return_conditional_losses_4822
reshape/PartitionedCall»
*fused_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall reshape/PartitionedCall:output:0fused_conv2d_re_lu_611*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:€€€€€€€€€*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *T
fORM
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_5182,
*fused_conv2d_re_lu/StatefulPartitionedCallю
flatten/PartitionedCallPartitionedCall3fused_conv2d_re_lu/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:€€€€€€€€€ * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *I
fDRB
@__inference_flatten_layer_call_and_return_conditional_losses_5432
flatten/PartitionedCallЈ
#fused_dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0fused_dense_615fused_dense_617*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:€€€€€€€€€*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *M
fHRF
D__inference_fused_dense_layer_call_and_return_conditional_losses_5792%
#fused_dense/StatefulPartitionedCallв
IdentityIdentity3fused_conv2d_re_lu/StatefulPartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

IdentityЋ

Identity_1Identity flatten/PartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity_1„

Identity_2Identity,fused_dense/StatefulPartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::2X
*fused_conv2d_re_lu/StatefulPartitionedCall*fused_conv2d_re_lu/StatefulPartitionedCall2J
#fused_dense/StatefulPartitionedCall#fused_dense/StatefulPartitionedCall:T P
+
_output_shapes
:€€€€€€€€€
!
_user_specified_name	input_1
–
y
)__inference_fused_dense_layer_call_fn_927
x
unknown
	unknown_0
identityИҐStatefulPartitionedCallт
StatefulPartitionedCallStatefulPartitionedCallxunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:€€€€€€€€€*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *M
fHRF
D__inference_fused_dense_layer_call_and_return_conditional_losses_5792
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€ ::22
StatefulPartitionedCallStatefulPartitionedCall:J F
'
_output_shapes
:€€€€€€€€€ 

_user_specified_namex
ƒ

І
#__inference_model_layer_call_fn_794

inputs
unknown
	unknown_0
	unknown_1
identity

identity_1

identity_2ИҐStatefulPartitionedCallЃ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *U
_output_shapesC
A:€€€€€€€€€:€€€€€€€€€ :€€€€€€€€€*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *G
fBR@
>__inference_model_layer_call_and_return_conditional_losses_6422
StatefulPartitionedCallЦ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

IdentityТ

Identity_1Identity StatefulPartitionedCall:output:1^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity_1Т

Identity_2Identity StatefulPartitionedCall:output:2^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs
У
Щ
__inference__traced_restore_980
file_prefix5
1assignvariableop_fused_conv2d_re_lu_conv2d_kernel/
+assignvariableop_1_fused_dense_dense_kernel-
)assignvariableop_2_fused_dense_dense_bias

identity_4ИҐAssignVariableOpҐAssignVariableOp_1ҐAssignVariableOp_2љ
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*…
valueњBЉB0trainable_variables/0/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/1/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_namesЦ
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueBB B B B 2
RestoreV2/shape_and_slicesњ
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*$
_output_shapes
::::*
dtypes
22
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity∞
AssignVariableOpAssignVariableOp1assignvariableop_fused_conv2d_re_lu_conv2d_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1∞
AssignVariableOp_1AssignVariableOp+assignvariableop_1_fused_dense_dense_kernelIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2Ѓ
AssignVariableOp_2AssignVariableOp)assignvariableop_2_fused_dense_dense_biasIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp•

Identity_3Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_3Ч

Identity_4IdentityIdentity_3:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_2*
T0*
_output_shapes
: 2

Identity_4"!

identity_4Identity_4:output:0*!
_input_shapes
: :::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12(
AssignVariableOp_2AssignVariableOp_2:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
–
y
)__inference_fused_dense_layer_call_fn_918
x
unknown
	unknown_0
identityИҐStatefulPartitionedCallт
StatefulPartitionedCallStatefulPartitionedCallxunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:€€€€€€€€€*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *M
fHRF
D__inference_fused_dense_layer_call_and_return_conditional_losses_5792
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€ ::22
StatefulPartitionedCallStatefulPartitionedCall:J F
'
_output_shapes
:€€€€€€€€€ 

_user_specified_namex
ƒ

І
#__inference_model_layer_call_fn_809

inputs
unknown
	unknown_0
	unknown_1
identity

identity_1

identity_2ИҐStatefulPartitionedCallЃ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *U
_output_shapesC
A:€€€€€€€€€:€€€€€€€€€ :€€€€€€€€€*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *G
fBR@
>__inference_model_layer_call_and_return_conditional_losses_6732
StatefulPartitionedCallЦ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

IdentityТ

Identity_1Identity StatefulPartitionedCall:output:1^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity_1Т

Identity_2Identity StatefulPartitionedCall:output:2^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::22
StatefulPartitionedCallStatefulPartitionedCall:S O
+
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs
£
Ч
>__inference_model_layer_call_and_return_conditional_losses_607
input_1
fused_conv2d_re_lu_534
fused_dense_599
fused_dense_601
identity

identity_1

identity_2ИҐ*fused_conv2d_re_lu/StatefulPartitionedCallҐ#fused_dense/StatefulPartitionedCallЏ
reshape/PartitionedCallPartitionedCallinput_1*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:€€€€€€€€€* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *I
fDRB
@__inference_reshape_layer_call_and_return_conditional_losses_4822
reshape/PartitionedCall»
*fused_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall reshape/PartitionedCall:output:0fused_conv2d_re_lu_534*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:€€€€€€€€€*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *T
fORM
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_5042,
*fused_conv2d_re_lu/StatefulPartitionedCallю
flatten/PartitionedCallPartitionedCall3fused_conv2d_re_lu/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:€€€€€€€€€ * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *I
fDRB
@__inference_flatten_layer_call_and_return_conditional_losses_5432
flatten/PartitionedCallЈ
#fused_dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0fused_dense_599fused_dense_601*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:€€€€€€€€€*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *M
fHRF
D__inference_fused_dense_layer_call_and_return_conditional_losses_5652%
#fused_dense/StatefulPartitionedCallв
IdentityIdentity3fused_conv2d_re_lu/StatefulPartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

IdentityЋ

Identity_1Identity flatten/PartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity_1„

Identity_2Identity,fused_dense/StatefulPartitionedCall:output:0+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::2X
*fused_conv2d_re_lu/StatefulPartitionedCall*fused_conv2d_re_lu/StatefulPartitionedCall2J
#fused_dense/StatefulPartitionedCall#fused_dense/StatefulPartitionedCall:T P
+
_output_shapes
:€€€€€€€€€
!
_user_specified_name	input_1
÷
ѓ
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_504
x)
%conv2d_conv2d_readvariableop_resource
identityИҐconv2d/Conv2D/ReadVariableOpЂ
zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d/Pad/paddingsТ
zero_padding2d/PadPadx$zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
zero_padding2d/Pad™
conv2d/Conv2D/ReadVariableOpReadVariableOp%conv2d_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
conv2d/Conv2D/ReadVariableOpќ
conv2d/Conv2DConv2Dzero_padding2d/Pad:output:0$conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€*
paddingVALID*
strides
2
conv2d/Conv2Dt
conv2d/ReluReluconv2d/Conv2D:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
conv2d/ReluГ
clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  А?2
clamp/clip_by_value/Minimum/y¬
clamp/clip_by_value/MinimumMinimumconv2d/Relu:activations:0&clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
clamp/clip_by_value/Minimums
clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  Ањ2
clamp/clip_by_value/y∞
clamp/clip_by_valueMaximumclamp/clip_by_value/Minimum:z:0clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
clamp/clip_by_valueТ
IdentityIdentityclamp/clip_by_value:z:0^conv2d/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€:2<
conv2d/Conv2D/ReadVariableOpconv2d/Conv2D/ReadVariableOp:R N
/
_output_shapes
:€€€€€€€€€

_user_specified_namex
Э
A
%__inference_flatten_layer_call_fn_881

inputs
identityЅ
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:€€€€€€€€€ * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *I
fDRB
@__inference_flatten_layer_call_and_return_conditional_losses_5432
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€:W S
/
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs
®
H
,__inference_zero_padding2d_layer_call_fn_464

inputs
identityл
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *P
fKRI
G__inference_zero_padding2d_layer_call_and_return_conditional_losses_4582
PartitionedCallП
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€:r n
J
_output_shapes8
6:4€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€
 
_user_specified_nameinputs
Н
р
D__inference_fused_dense_layer_call_and_return_conditional_losses_909
x(
$dense_matmul_readvariableop_resource)
%dense_biasadd_readvariableop_resource
identityИҐdense/BiasAdd/ReadVariableOpҐdense/MatMul/ReadVariableOpЯ
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

: *
dtype02
dense/MatMul/ReadVariableOpА
dense/MatMulMatMulx#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
dense/MatMulЮ
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
dense/BiasAdd/ReadVariableOpЩ
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
dense/BiasAddЗ
clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2!
clamp_1/clip_by_value/Minimum/yљ
clamp_1/clip_by_value/MinimumMinimumdense/BiasAdd:output:0(clamp_1/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2
clamp_1/clip_by_value/Minimumw
clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   »2
clamp_1/clip_by_value/y∞
clamp_1/clip_by_valueMaximum!clamp_1/clip_by_value/Minimum:z:0 clamp_1/clip_by_value/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2
clamp_1/clip_by_value™
IdentityIdentityclamp_1/clip_by_value:z:0^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€ ::2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp:J F
'
_output_shapes
:€€€€€€€€€ 

_user_specified_namex
Н
р
D__inference_fused_dense_layer_call_and_return_conditional_losses_579
x(
$dense_matmul_readvariableop_resource)
%dense_biasadd_readvariableop_resource
identityИҐdense/BiasAdd/ReadVariableOpҐdense/MatMul/ReadVariableOpЯ
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

: *
dtype02
dense/MatMul/ReadVariableOpА
dense/MatMulMatMulx#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
dense/MatMulЮ
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
dense/BiasAdd/ReadVariableOpЩ
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
dense/BiasAddЗ
clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2!
clamp_1/clip_by_value/Minimum/yљ
clamp_1/clip_by_value/MinimumMinimumdense/BiasAdd:output:0(clamp_1/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2
clamp_1/clip_by_value/Minimumw
clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   »2
clamp_1/clip_by_value/y∞
clamp_1/clip_by_valueMaximum!clamp_1/clip_by_value/Minimum:z:0 clamp_1/clip_by_value/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2
clamp_1/clip_by_value™
IdentityIdentityclamp_1/clip_by_value:z:0^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€ ::2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp:J F
'
_output_shapes
:€€€€€€€€€ 

_user_specified_namex
ё
q
0__inference_fused_conv2d_re_lu_layer_call_fn_863
x
unknown
identityИҐStatefulPartitionedCallф
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:€€€€€€€€€*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *T
fORM
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_5182
StatefulPartitionedCallЦ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€:22
StatefulPartitionedCallStatefulPartitionedCall:R N
/
_output_shapes
:€€€€€€€€€

_user_specified_namex
ґ
\
@__inference_flatten_layer_call_and_return_conditional_losses_543

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"€€€€    2
Constg
ReshapeReshapeinputsConst:output:0*
T0*'
_output_shapes
:€€€€€€€€€ 2	
Reshaped
IdentityIdentityReshape:output:0*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€:W S
/
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs
£7
ѓ
>__inference_model_layer_call_and_return_conditional_losses_741

inputs<
8fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resource4
0fused_dense_dense_matmul_readvariableop_resource5
1fused_dense_dense_biasadd_readvariableop_resource
identity

identity_1

identity_2ИҐ/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOpҐ(fused_dense/dense/BiasAdd/ReadVariableOpҐ'fused_dense/dense/MatMul/ReadVariableOpT
reshape/ShapeShapeinputs*
T0*
_output_shapes
:2
reshape/ShapeД
reshape/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
reshape/strided_slice/stackИ
reshape/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
reshape/strided_slice/stack_1И
reshape/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
reshape/strided_slice/stack_2Т
reshape/strided_sliceStridedSlicereshape/Shape:output:0$reshape/strided_slice/stack:output:0&reshape/strided_slice/stack_1:output:0&reshape/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
reshape/strided_slicet
reshape/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
reshape/Reshape/shape/1t
reshape/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
reshape/Reshape/shape/2t
reshape/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
reshape/Reshape/shape/3к
reshape/Reshape/shapePackreshape/strided_slice:output:0 reshape/Reshape/shape/1:output:0 reshape/Reshape/shape/2:output:0 reshape/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
reshape/Reshape/shapeП
reshape/ReshapeReshapeinputsreshape/Reshape/shape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
reshape/Reshape—
.fused_conv2d_re_lu/zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             20
.fused_conv2d_re_lu/zero_padding2d/Pad/paddingsв
%fused_conv2d_re_lu/zero_padding2d/PadPadreshape/Reshape:output:07fused_conv2d_re_lu/zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€2'
%fused_conv2d_re_lu/zero_padding2d/Padг
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOpReadVariableOp8fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype021
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOpЪ
 fused_conv2d_re_lu/conv2d/Conv2DConv2D.fused_conv2d_re_lu/zero_padding2d/Pad:output:07fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€*
paddingVALID*
strides
2"
 fused_conv2d_re_lu/conv2d/Conv2D≠
fused_conv2d_re_lu/conv2d/ReluRelu)fused_conv2d_re_lu/conv2d/Conv2D:output:0*
T0*/
_output_shapes
:€€€€€€€€€2 
fused_conv2d_re_lu/conv2d/Relu©
0fused_conv2d_re_lu/clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  А?22
0fused_conv2d_re_lu/clamp/clip_by_value/Minimum/yО
.fused_conv2d_re_lu/clamp/clip_by_value/MinimumMinimum,fused_conv2d_re_lu/conv2d/Relu:activations:09fused_conv2d_re_lu/clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€20
.fused_conv2d_re_lu/clamp/clip_by_value/MinimumЩ
(fused_conv2d_re_lu/clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  Ањ2*
(fused_conv2d_re_lu/clamp/clip_by_value/yь
&fused_conv2d_re_lu/clamp/clip_by_valueMaximum2fused_conv2d_re_lu/clamp/clip_by_value/Minimum:z:01fused_conv2d_re_lu/clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€2(
&fused_conv2d_re_lu/clamp/clip_by_valueo
flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"€€€€    2
flatten/Const£
flatten/ReshapeReshape*fused_conv2d_re_lu/clamp/clip_by_value:z:0flatten/Const:output:0*
T0*'
_output_shapes
:€€€€€€€€€ 2
flatten/Reshape√
'fused_dense/dense/MatMul/ReadVariableOpReadVariableOp0fused_dense_dense_matmul_readvariableop_resource*
_output_shapes

: *
dtype02)
'fused_dense/dense/MatMul/ReadVariableOpї
fused_dense/dense/MatMulMatMulflatten/Reshape:output:0/fused_dense/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
fused_dense/dense/MatMul¬
(fused_dense/dense/BiasAdd/ReadVariableOpReadVariableOp1fused_dense_dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02*
(fused_dense/dense/BiasAdd/ReadVariableOp…
fused_dense/dense/BiasAddBiasAdd"fused_dense/dense/MatMul:product:00fused_dense/dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
fused_dense/dense/BiasAddЯ
+fused_dense/clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2-
+fused_dense/clamp_1/clip_by_value/Minimum/yн
)fused_dense/clamp_1/clip_by_value/MinimumMinimum"fused_dense/dense/BiasAdd:output:04fused_dense/clamp_1/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2+
)fused_dense/clamp_1/clip_by_value/MinimumП
#fused_dense/clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   »2%
#fused_dense/clamp_1/clip_by_value/yа
!fused_dense/clamp_1/clip_by_valueMaximum-fused_dense/clamp_1/clip_by_value/Minimum:z:0,fused_dense/clamp_1/clip_by_value/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2#
!fused_dense/clamp_1/clip_by_valueН
IdentityIdentity*fused_conv2d_re_lu/clamp/clip_by_value:z:00^fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp)^fused_dense/dense/BiasAdd/ReadVariableOp(^fused_dense/dense/MatMul/ReadVariableOp*
T0*/
_output_shapes
:€€€€€€€€€2

Identityч

Identity_1Identityflatten/Reshape:output:00^fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp)^fused_dense/dense/BiasAdd/ReadVariableOp(^fused_dense/dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity_1Д

Identity_2Identity%fused_dense/clamp_1/clip_by_value:z:00^fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp)^fused_dense/dense/BiasAdd/ReadVariableOp(^fused_dense/dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::2b
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp2T
(fused_dense/dense/BiasAdd/ReadVariableOp(fused_dense/dense/BiasAdd/ReadVariableOp2R
'fused_dense/dense/MatMul/ReadVariableOp'fused_dense/dense/MatMul/ReadVariableOp:S O
+
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs
∞<
і
__inference__wrapped_model_451
input_1B
>model_fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resource:
6model_fused_dense_dense_matmul_readvariableop_resource;
7model_fused_dense_dense_biasadd_readvariableop_resource
identity

identity_1

identity_2ИҐ5model/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOpҐ.model/fused_dense/dense/BiasAdd/ReadVariableOpҐ-model/fused_dense/dense/MatMul/ReadVariableOpa
model/reshape/ShapeShapeinput_1*
T0*
_output_shapes
:2
model/reshape/ShapeР
!model/reshape/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2#
!model/reshape/strided_slice/stackФ
#model/reshape/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2%
#model/reshape/strided_slice/stack_1Ф
#model/reshape/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2%
#model/reshape/strided_slice/stack_2ґ
model/reshape/strided_sliceStridedSlicemodel/reshape/Shape:output:0*model/reshape/strided_slice/stack:output:0,model/reshape/strided_slice/stack_1:output:0,model/reshape/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
model/reshape/strided_sliceА
model/reshape/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
model/reshape/Reshape/shape/1А
model/reshape/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
model/reshape/Reshape/shape/2А
model/reshape/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
model/reshape/Reshape/shape/3О
model/reshape/Reshape/shapePack$model/reshape/strided_slice:output:0&model/reshape/Reshape/shape/1:output:0&model/reshape/Reshape/shape/2:output:0&model/reshape/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
model/reshape/Reshape/shapeҐ
model/reshape/ReshapeReshapeinput_1$model/reshape/Reshape/shape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
model/reshape/ReshapeЁ
4model/fused_conv2d_re_lu/zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             26
4model/fused_conv2d_re_lu/zero_padding2d/Pad/paddingsъ
+model/fused_conv2d_re_lu/zero_padding2d/PadPadmodel/reshape/Reshape:output:0=model/fused_conv2d_re_lu/zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€2-
+model/fused_conv2d_re_lu/zero_padding2d/Padх
5model/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOpReadVariableOp>model_fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype027
5model/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp≤
&model/fused_conv2d_re_lu/conv2d/Conv2DConv2D4model/fused_conv2d_re_lu/zero_padding2d/Pad:output:0=model/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€*
paddingVALID*
strides
2(
&model/fused_conv2d_re_lu/conv2d/Conv2Dњ
$model/fused_conv2d_re_lu/conv2d/ReluRelu/model/fused_conv2d_re_lu/conv2d/Conv2D:output:0*
T0*/
_output_shapes
:€€€€€€€€€2&
$model/fused_conv2d_re_lu/conv2d/Reluµ
6model/fused_conv2d_re_lu/clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  А?28
6model/fused_conv2d_re_lu/clamp/clip_by_value/Minimum/y¶
4model/fused_conv2d_re_lu/clamp/clip_by_value/MinimumMinimum2model/fused_conv2d_re_lu/conv2d/Relu:activations:0?model/fused_conv2d_re_lu/clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€26
4model/fused_conv2d_re_lu/clamp/clip_by_value/Minimum•
.model/fused_conv2d_re_lu/clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  Ањ20
.model/fused_conv2d_re_lu/clamp/clip_by_value/yФ
,model/fused_conv2d_re_lu/clamp/clip_by_valueMaximum8model/fused_conv2d_re_lu/clamp/clip_by_value/Minimum:z:07model/fused_conv2d_re_lu/clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€2.
,model/fused_conv2d_re_lu/clamp/clip_by_value{
model/flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"€€€€    2
model/flatten/Constї
model/flatten/ReshapeReshape0model/fused_conv2d_re_lu/clamp/clip_by_value:z:0model/flatten/Const:output:0*
T0*'
_output_shapes
:€€€€€€€€€ 2
model/flatten/Reshape’
-model/fused_dense/dense/MatMul/ReadVariableOpReadVariableOp6model_fused_dense_dense_matmul_readvariableop_resource*
_output_shapes

: *
dtype02/
-model/fused_dense/dense/MatMul/ReadVariableOp”
model/fused_dense/dense/MatMulMatMulmodel/flatten/Reshape:output:05model/fused_dense/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2 
model/fused_dense/dense/MatMul‘
.model/fused_dense/dense/BiasAdd/ReadVariableOpReadVariableOp7model_fused_dense_dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype020
.model/fused_dense/dense/BiasAdd/ReadVariableOpб
model/fused_dense/dense/BiasAddBiasAdd(model/fused_dense/dense/MatMul:product:06model/fused_dense/dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2!
model/fused_dense/dense/BiasAddЂ
1model/fused_dense/clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H23
1model/fused_dense/clamp_1/clip_by_value/Minimum/yЕ
/model/fused_dense/clamp_1/clip_by_value/MinimumMinimum(model/fused_dense/dense/BiasAdd:output:0:model/fused_dense/clamp_1/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€21
/model/fused_dense/clamp_1/clip_by_value/MinimumЫ
)model/fused_dense/clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   »2+
)model/fused_dense/clamp_1/clip_by_value/yш
'model/fused_dense/clamp_1/clip_by_valueMaximum3model/fused_dense/clamp_1/clip_by_value/Minimum:z:02model/fused_dense/clamp_1/clip_by_value/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2)
'model/fused_dense/clamp_1/clip_by_valueЛ
IdentityIdentitymodel/flatten/Reshape:output:06^model/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp/^model/fused_dense/dense/BiasAdd/ReadVariableOp.^model/fused_dense/dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity©

Identity_1Identity0model/fused_conv2d_re_lu/clamp/clip_by_value:z:06^model/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp/^model/fused_dense/dense/BiasAdd/ReadVariableOp.^model/fused_dense/dense/MatMul/ReadVariableOp*
T0*/
_output_shapes
:€€€€€€€€€2

Identity_1Ь

Identity_2Identity+model/fused_dense/clamp_1/clip_by_value:z:06^model/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp/^model/fused_dense/dense/BiasAdd/ReadVariableOp.^model/fused_dense/dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::2n
5model/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp5model/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp2`
.model/fused_dense/dense/BiasAdd/ReadVariableOp.model/fused_dense/dense/BiasAdd/ReadVariableOp2^
-model/fused_dense/dense/MatMul/ReadVariableOp-model/fused_dense/dense/MatMul/ReadVariableOp:T P
+
_output_shapes
:€€€€€€€€€
!
_user_specified_name	input_1
÷
ѓ
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_842
x)
%conv2d_conv2d_readvariableop_resource
identityИҐconv2d/Conv2D/ReadVariableOpЂ
zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d/Pad/paddingsТ
zero_padding2d/PadPadx$zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
zero_padding2d/Pad™
conv2d/Conv2D/ReadVariableOpReadVariableOp%conv2d_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype02
conv2d/Conv2D/ReadVariableOpќ
conv2d/Conv2DConv2Dzero_padding2d/Pad:output:0$conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:€€€€€€€€€*
paddingVALID*
strides
2
conv2d/Conv2Dt
conv2d/ReluReluconv2d/Conv2D:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
conv2d/ReluГ
clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  А?2
clamp/clip_by_value/Minimum/y¬
clamp/clip_by_value/MinimumMinimumconv2d/Relu:activations:0&clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
clamp/clip_by_value/Minimums
clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  Ањ2
clamp/clip_by_value/y∞
clamp/clip_by_valueMaximumclamp/clip_by_value/Minimum:z:0clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:€€€€€€€€€2
clamp/clip_by_valueТ
IdentityIdentityclamp/clip_by_value:z:0^conv2d/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€:2<
conv2d/Conv2D/ReadVariableOpconv2d/Conv2D/ReadVariableOp:R N
/
_output_shapes
:€€€€€€€€€

_user_specified_namex
й
\
@__inference_reshape_layer_call_and_return_conditional_losses_482

inputs
identityD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2в
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_sliced
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/1d
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/2d
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value	B :2
Reshape/shape/3Ї
Reshape/shapePackstrided_slice:output:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:2
Reshape/shapew
ReshapeReshapeinputsReshape/shape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2	
Reshapel
IdentityIdentityReshape:output:0*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0**
_input_shapes
:€€€€€€€€€:S O
+
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs
ё
q
0__inference_fused_conv2d_re_lu_layer_call_fn_870
x
unknown
identityИҐStatefulPartitionedCallф
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:€€€€€€€€€*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *T
fORM
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_5182
StatefulPartitionedCallЦ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*2
_input_shapes!
:€€€€€€€€€:22
StatefulPartitionedCallStatefulPartitionedCall:R N
/
_output_shapes
:€€€€€€€€€

_user_specified_namex
«

®
#__inference_model_layer_call_fn_686
input_1
unknown
	unknown_0
	unknown_1
identity

identity_1

identity_2ИҐStatefulPartitionedCallѓ
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *U
_output_shapesC
A:€€€€€€€€€:€€€€€€€€€ :€€€€€€€€€*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *G
fBR@
>__inference_model_layer_call_and_return_conditional_losses_6732
StatefulPartitionedCallЦ
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

IdentityТ

Identity_1Identity StatefulPartitionedCall:output:1^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€ 2

Identity_1Т

Identity_2Identity StatefulPartitionedCall:output:2^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::22
StatefulPartitionedCallStatefulPartitionedCall:T P
+
_output_shapes
:€€€€€€€€€
!
_user_specified_name	input_1
г
Ъ
__inference__traced_save_961
file_prefix?
;savev2_fused_conv2d_re_lu_conv2d_kernel_read_readvariableop7
3savev2_fused_dense_dense_kernel_read_readvariableop5
1savev2_fused_dense_dense_bias_read_readvariableop
savev2_const

identity_1ИҐMergeV2CheckpointsП
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1Л
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard¶
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilenameЈ
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*…
valueњBЉB0trainable_variables/0/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/1/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_namesР
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueBB B B B 2
SaveV2/shape_and_slicesв
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0;savev2_fused_conv2d_re_lu_conv2d_kernel_read_readvariableop3savev2_fused_dense_dense_kernel_read_readvariableop1savev2_fused_dense_dense_bias_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *
dtypes
22
SaveV2Ї
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes°
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*9
_input_shapes(
&: :: :: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:,(
&
_output_shapes
::$ 

_output_shapes

: : 

_output_shapes
::

_output_shapes
: 
•

¶
!__inference_signature_wrapper_703
input_1
unknown
	unknown_0
	unknown_1
identity

identity_1

identity_2ИҐStatefulPartitionedCallП
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *U
_output_shapesC
A:€€€€€€€€€ :€€€€€€€€€:€€€€€€€€€*%
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8В *'
f"R 
__inference__wrapped_model_4512
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€ 2

IdentityЪ

Identity_1Identity StatefulPartitionedCall:output:1^StatefulPartitionedCall*
T0*/
_output_shapes
:€€€€€€€€€2

Identity_1Т

Identity_2Identity StatefulPartitionedCall:output:2^StatefulPartitionedCall*
T0*'
_output_shapes
:€€€€€€€€€2

Identity_2"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*6
_input_shapes%
#:€€€€€€€€€:::22
StatefulPartitionedCallStatefulPartitionedCall:T P
+
_output_shapes
:€€€€€€€€€
!
_user_specified_name	input_1
Н
р
D__inference_fused_dense_layer_call_and_return_conditional_losses_565
x(
$dense_matmul_readvariableop_resource)
%dense_biasadd_readvariableop_resource
identityИҐdense/BiasAdd/ReadVariableOpҐdense/MatMul/ReadVariableOpЯ
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

: *
dtype02
dense/MatMul/ReadVariableOpА
dense/MatMulMatMulx#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
dense/MatMulЮ
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02
dense/BiasAdd/ReadVariableOpЩ
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:€€€€€€€€€2
dense/BiasAddЗ
clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2!
clamp_1/clip_by_value/Minimum/yљ
clamp_1/clip_by_value/MinimumMinimumdense/BiasAdd:output:0(clamp_1/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2
clamp_1/clip_by_value/Minimumw
clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   »2
clamp_1/clip_by_value/y∞
clamp_1/clip_by_valueMaximum!clamp_1/clip_by_value/Minimum:z:0 clamp_1/clip_by_value/y:output:0*
T0*'
_output_shapes
:€€€€€€€€€2
clamp_1/clip_by_value™
IdentityIdentityclamp_1/clip_by_value:z:0^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0*.
_input_shapes
:€€€€€€€€€ ::2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp:J F
'
_output_shapes
:€€€€€€€€€ 

_user_specified_namex
•
A
%__inference_reshape_layer_call_fn_828

inputs
identity…
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:€€€€€€€€€* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8В *I
fDRB
@__inference_reshape_layer_call_and_return_conditional_losses_4822
PartitionedCallt
IdentityIdentityPartitionedCall:output:0*
T0*/
_output_shapes
:€€€€€€€€€2

Identity"
identityIdentity:output:0**
_input_shapes
:€€€€€€€€€:S O
+
_output_shapes
:€€€€€€€€€
 
_user_specified_nameinputs"±L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*њ
serving_defaultЂ
?
input_14
serving_default_input_1:0€€€€€€€€€;
flatten0
StatefulPartitionedCall:0€€€€€€€€€ N
fused_conv2d_re_lu8
StatefulPartitionedCall:1€€€€€€€€€?
fused_dense0
StatefulPartitionedCall:2€€€€€€€€€tensorflow/serving/predict:чи
о
layer-0
layer-1
layer_with_weights-0
layer-2
layer-3
layer_with_weights-1
layer-4
	optimizer
trainable_variables
	variables
	regularization_losses

	keras_api

signatures
*r&call_and_return_all_conditional_losses
s__call__
t_default_save_signature"Ѓ
_tf_keras_networkТ{"class_name": "Functional", "name": "model", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "config": {"name": "model", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 4, 4]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_1"}, "name": "input_1", "inbound_nodes": []}, {"class_name": "Reshape", "config": {"name": "reshape", "trainable": true, "dtype": "float32", "target_shape": {"class_name": "__tuple__", "items": [4, 4, 1]}}, "name": "reshape", "inbound_nodes": [[["input_1", 0, 0, {}]]]}, {"class_name": "FusedConv2DReLU", "config": {"name": "FusedConv2DReLU", "filters": 2, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}, "name": "fused_conv2d_re_lu", "inbound_nodes": [[["reshape", 0, 0, {}]]]}, {"class_name": "Flatten", "config": {"name": "flatten", "trainable": true, "dtype": "float32", "data_format": "channels_last"}, "name": "flatten", "inbound_nodes": [[["fused_conv2d_re_lu", 0, 0, {}]]]}, {"class_name": "FusedDense", "config": {"name": "FusedDense", "units": 5, "activation": null, "use_bias": true}, "name": "fused_dense", "inbound_nodes": [[["flatten", 0, 0, {}]]]}], "input_layers": [["input_1", 0, 0]], "output_layers": [["fused_conv2d_re_lu", 0, 0], ["flatten", 0, 0], ["fused_dense", 0, 0]]}, "input_spec": [{"class_name": "InputSpec", "config": {"dtype": null, "shape": {"class_name": "__tuple__", "items": [null, 4, 4]}, "ndim": 3, "max_ndim": null, "min_ndim": null, "axes": {}}}], "build_input_shape": {"class_name": "TensorShape", "items": [null, 4, 4]}, "is_graph_network": true, "keras_version": "2.4.0", "backend": "tensorflow", "model_config": {"class_name": "Functional", "config": {"name": "model", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 4, 4]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_1"}, "name": "input_1", "inbound_nodes": []}, {"class_name": "Reshape", "config": {"name": "reshape", "trainable": true, "dtype": "float32", "target_shape": {"class_name": "__tuple__", "items": [4, 4, 1]}}, "name": "reshape", "inbound_nodes": [[["input_1", 0, 0, {}]]]}, {"class_name": "FusedConv2DReLU", "config": {"name": "FusedConv2DReLU", "filters": 2, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}, "name": "fused_conv2d_re_lu", "inbound_nodes": [[["reshape", 0, 0, {}]]]}, {"class_name": "Flatten", "config": {"name": "flatten", "trainable": true, "dtype": "float32", "data_format": "channels_last"}, "name": "flatten", "inbound_nodes": [[["fused_conv2d_re_lu", 0, 0, {}]]]}, {"class_name": "FusedDense", "config": {"name": "FusedDense", "units": 5, "activation": null, "use_bias": true}, "name": "fused_dense", "inbound_nodes": [[["flatten", 0, 0, {}]]]}], "input_layers": [["input_1", 0, 0]], "output_layers": [["fused_conv2d_re_lu", 0, 0], ["flatten", 0, 0], ["fused_dense", 0, 0]]}}, "training_config": {"loss": {"class_name": "SparseCategoricalCrossentropy", "config": {"reduction": "auto", "name": "sparse_categorical_crossentropy", "from_logits": true}}, "metrics": ["accuracy"], "weighted_metrics": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.001, "decay": 0.0, "beta_1": 0.9, "beta_2": 0.999, "epsilon": 1e-07, "amsgrad": false}}}}
п"м
_tf_keras_input_layerћ{"class_name": "InputLayer", "name": "input_1", "dtype": "float32", "sparse": false, "ragged": false, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 4, 4]}, "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 4, 4]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_1"}}
у
trainable_variables
	variables
regularization_losses
	keras_api
*u&call_and_return_all_conditional_losses
v__call__"д
_tf_keras_layer {"class_name": "Reshape", "name": "reshape", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "reshape", "trainable": true, "dtype": "float32", "target_shape": {"class_name": "__tuple__", "items": [4, 4, 1]}}}
÷
zeropadding

conv2d
quantize_pool

clamp_pool
quantize
	clamp
trainable_variables
	variables
regularization_losses
	keras_api
*w&call_and_return_all_conditional_losses
x__call__"о
_tf_keras_layer‘{"class_name": "FusedConv2DReLU", "name": "fused_conv2d_re_lu", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "FusedConv2DReLU", "filters": 2, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}
в
trainable_variables
	variables
regularization_losses
	keras_api
*y&call_and_return_all_conditional_losses
z__call__"”
_tf_keras_layerє{"class_name": "Flatten", "name": "flatten", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "flatten", "trainable": true, "dtype": "float32", "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 1, "axes": {}}}}
л
	Dense
quantize
	 clamp
!trainable_variables
"	variables
#regularization_losses
$	keras_api
*{&call_and_return_all_conditional_losses
|__call__"Є
_tf_keras_layerЮ{"class_name": "FusedDense", "name": "fused_dense", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "FusedDense", "units": 5, "activation": null, "use_bias": true}}
"
	optimizer
5
%0
&1
'2"
trackable_list_wrapper
5
%0
&1
'2"
trackable_list_wrapper
 "
trackable_list_wrapper
 
(non_trainable_variables
)layer_metrics
trainable_variables
	variables

*layers
+metrics
	regularization_losses
,layer_regularization_losses
s__call__
t_default_save_signature
*r&call_and_return_all_conditional_losses
&r"call_and_return_conditional_losses"
_generic_user_object
,
}serving_default"
signature_map
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
≠
-layer_metrics
.non_trainable_variables
trainable_variables
	variables

/layers
0metrics
regularization_losses
1layer_regularization_losses
v__call__
*u&call_and_return_all_conditional_losses
&u"call_and_return_conditional_losses"
_generic_user_object
Е
2trainable_variables
3	variables
4regularization_losses
5	keras_api
*~&call_and_return_all_conditional_losses
__call__"ц
_tf_keras_layer№{"class_name": "ZeroPadding2D", "name": "zero_padding2d", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "zero_padding2d", "trainable": true, "dtype": "float32", "padding": {"class_name": "__tuple__", "items": [{"class_name": "__tuple__", "items": [1, 1]}, {"class_name": "__tuple__", "items": [1, 1]}]}, "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {}}}}
Ь

%kernel
6trainable_variables
7	variables
8regularization_losses
9	keras_api
+А&call_and_return_all_conditional_losses
Б__call__"€	
_tf_keras_layerе	{"class_name": "Conv2D", "name": "conv2d", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "conv2d", "trainable": true, "dtype": "float32", "filters": 2, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "valid", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "relu", "use_bias": false, "kernel_initializer": {"class_name": "Constant", "config": {"value": [-0.8984375, -0.796875, -0.6875, -0.5859375, -0.4765625, -0.3671875, -0.265625, -0.15625, -0.0546875, 0.0546875, 0.15625, 0.265625, 0.3671875, 0.4765625, 0.5859375, 0.6875, 0.796875, 0.8984375]}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 1}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 6, 6, 1]}}
ѓ
:	keras_api"Э
_tf_keras_layerГ{"class_name": "Empty", "name": "empty", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty", "trainable": true, "dtype": "float32"}}
≥
;	keras_api"°
_tf_keras_layerЗ{"class_name": "Empty", "name": "empty_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_1", "trainable": true, "dtype": "float32"}}
≥
<	keras_api"°
_tf_keras_layerЗ{"class_name": "Empty", "name": "empty_2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_2", "trainable": true, "dtype": "float32"}}
І
=trainable_variables
>	variables
?regularization_losses
@	keras_api
+В&call_and_return_all_conditional_losses
Г__call__"Ц
_tf_keras_layerь{"class_name": "Clamp", "name": "clamp", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "Clamp", "min_val": -1.0, "max_val": 1.0}}
'
%0"
trackable_list_wrapper
'
%0"
trackable_list_wrapper
 "
trackable_list_wrapper
≠
Alayer_metrics
Bnon_trainable_variables
trainable_variables
	variables

Clayers
Dmetrics
regularization_losses
Elayer_regularization_losses
x__call__
*w&call_and_return_all_conditional_losses
&w"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
≠
Flayer_metrics
Gnon_trainable_variables
trainable_variables
	variables

Hlayers
Imetrics
regularization_losses
Jlayer_regularization_losses
z__call__
*y&call_and_return_all_conditional_losses
&y"call_and_return_conditional_losses"
_generic_user_object
ц

&kernel
'bias
Ktrainable_variables
L	variables
Mregularization_losses
N	keras_api
+Д&call_and_return_all_conditional_losses
Е__call__"ѕ
_tf_keras_layerµ{"class_name": "Dense", "name": "dense", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense", "trainable": true, "dtype": "float32", "units": 5, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "Constant", "config": {"value": [-0.5, -0.4921875, -0.484375, -0.484375, -0.4765625, -0.46875, -0.4609375, -0.453125, -0.453125, -0.4453125, -0.4375, -0.4296875, -0.421875, -0.421875, -0.4140625, -0.40625, -0.3984375, -0.390625, -0.390625, -0.3828125, -0.375, -0.3671875, -0.359375, -0.3515625, -0.3515625, -0.34375, -0.3359375, -0.328125, -0.3203125, -0.3203125, -0.3125, -0.3046875, -0.296875, -0.2890625, -0.2890625, -0.28125, -0.2734375, -0.265625, -0.2578125, -0.2578125, -0.25, -0.2421875, -0.234375, -0.2265625, -0.2265625, -0.21875, -0.2109375, -0.203125, -0.1953125, -0.1953125, -0.1875, -0.1796875, -0.171875, -0.1640625, -0.1640625, -0.15625, -0.1484375, -0.140625, -0.1328125, -0.1328125, -0.125, -0.1171875, -0.109375, -0.1015625, -0.09375, -0.09375, -0.0859375, -0.078125, -0.0703125, -0.0625, -0.0625, -0.0546875, -0.046875, -0.0390625, -0.03125, -0.03125, -0.0234375, -0.015625, -0.0078125, 0.0, 0.0, 0.0078125, 0.015625, 0.0234375, 0.03125, 0.03125, 0.0390625, 0.046875, 0.0546875, 0.0625, 0.0625, 0.0703125, 0.078125, 0.0859375, 0.09375, 0.09375, 0.1015625, 0.109375, 0.1171875, 0.125, 0.1328125, 0.1328125, 0.140625, 0.1484375, 0.15625, 0.1640625, 0.1640625, 0.171875, 0.1796875, 0.1875, 0.1953125, 0.1953125, 0.203125, 0.2109375, 0.21875, 0.2265625, 0.2265625, 0.234375, 0.2421875, 0.25, 0.2578125, 0.2578125, 0.265625, 0.2734375, 0.28125, 0.2890625, 0.2890625, 0.296875, 0.3046875, 0.3125, 0.3203125, 0.3203125, 0.328125, 0.3359375, 0.34375, 0.3515625, 0.3515625, 0.359375, 0.3671875, 0.375, 0.3828125, 0.390625, 0.390625, 0.3984375, 0.40625, 0.4140625, 0.421875, 0.421875, 0.4296875, 0.4375, 0.4453125, 0.453125, 0.453125, 0.4609375, 0.46875, 0.4765625, 0.484375, 0.484375, 0.4921875, 0.5]}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 32}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 32]}}
≥
O	keras_api"°
_tf_keras_layerЗ{"class_name": "Empty", "name": "empty_3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_3", "trainable": true, "dtype": "float32"}}
≥
Ptrainable_variables
Q	variables
Rregularization_losses
S	keras_api
+Ж&call_and_return_all_conditional_losses
З__call__"Ґ
_tf_keras_layerИ{"class_name": "Clamp", "name": "clamp_1", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "Clamp", "min_val": -131072.0, "max_val": 131072.0}}
.
&0
'1"
trackable_list_wrapper
.
&0
'1"
trackable_list_wrapper
 "
trackable_list_wrapper
≠
Tlayer_metrics
Unon_trainable_variables
!trainable_variables
"	variables

Vlayers
Wmetrics
#regularization_losses
Xlayer_regularization_losses
|__call__
*{&call_and_return_all_conditional_losses
&{"call_and_return_conditional_losses"
_generic_user_object
::82 fused_conv2d_re_lu/conv2d/kernel
*:( 2fused_dense/dense/kernel
$:"2fused_dense/dense/bias
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
C
0
1
2
3
4"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
≠
Ylayer_metrics
Znon_trainable_variables
2trainable_variables
3	variables

[layers
\metrics
4regularization_losses
]layer_regularization_losses
__call__
*~&call_and_return_all_conditional_losses
&~"call_and_return_conditional_losses"
_generic_user_object
'
%0"
trackable_list_wrapper
'
%0"
trackable_list_wrapper
 "
trackable_list_wrapper
∞
^layer_metrics
_non_trainable_variables
6trainable_variables
7	variables

`layers
ametrics
8regularization_losses
blayer_regularization_losses
Б__call__
+А&call_and_return_all_conditional_losses
'А"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
∞
clayer_metrics
dnon_trainable_variables
=trainable_variables
>	variables

elayers
fmetrics
?regularization_losses
glayer_regularization_losses
Г__call__
+В&call_and_return_all_conditional_losses
'В"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
J
0
1
2
3
4
5"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
&0
'1"
trackable_list_wrapper
.
&0
'1"
trackable_list_wrapper
 "
trackable_list_wrapper
∞
hlayer_metrics
inon_trainable_variables
Ktrainable_variables
L	variables

jlayers
kmetrics
Mregularization_losses
llayer_regularization_losses
Е__call__
+Д&call_and_return_all_conditional_losses
'Д"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
∞
mlayer_metrics
nnon_trainable_variables
Ptrainable_variables
Q	variables

olayers
pmetrics
Rregularization_losses
qlayer_regularization_losses
З__call__
+Ж&call_and_return_all_conditional_losses
'Ж"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
5
0
1
 2"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
∆2√
>__inference_model_layer_call_and_return_conditional_losses_779
>__inference_model_layer_call_and_return_conditional_losses_607
>__inference_model_layer_call_and_return_conditional_losses_623
>__inference_model_layer_call_and_return_conditional_losses_741ј
Ј≤≥
FullArgSpec1
args)Ъ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsЪ
p 

 

kwonlyargsЪ 
kwonlydefaults™ 
annotations™ *
 
Џ2„
#__inference_model_layer_call_fn_686
#__inference_model_layer_call_fn_794
#__inference_model_layer_call_fn_809
#__inference_model_layer_call_fn_655ј
Ј≤≥
FullArgSpec1
args)Ъ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsЪ
p 

 

kwonlyargsЪ 
kwonlydefaults™ 
annotations™ *
 
а2Ё
__inference__wrapped_model_451Ї
Л≤З
FullArgSpec
argsЪ 
varargsjargs
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ **Ґ'
%К"
input_1€€€€€€€€€
к2з
@__inference_reshape_layer_call_and_return_conditional_losses_823Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
ѕ2ћ
%__inference_reshape_layer_call_fn_828Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
ќ2Ћ
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_842
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_856Ѓ
•≤°
FullArgSpec$
argsЪ
jself
jx

jtraining
varargs
 
varkw
 
defaultsЪ
p 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
Ш2Х
0__inference_fused_conv2d_re_lu_layer_call_fn_870
0__inference_fused_conv2d_re_lu_layer_call_fn_863Ѓ
•≤°
FullArgSpec$
argsЪ
jself
jx

jtraining
varargs
 
varkw
 
defaultsЪ
p 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
к2з
@__inference_flatten_layer_call_and_return_conditional_losses_876Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
ѕ2ћ
%__inference_flatten_layer_call_fn_881Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
ј2љ
D__inference_fused_dense_layer_call_and_return_conditional_losses_895
D__inference_fused_dense_layer_call_and_return_conditional_losses_909Ѓ
•≤°
FullArgSpec$
argsЪ
jself
jx

jtraining
varargs
 
varkw
 
defaultsЪ
p 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
К2З
)__inference_fused_dense_layer_call_fn_927
)__inference_fused_dense_layer_call_fn_918Ѓ
•≤°
FullArgSpec$
argsЪ
jself
jx

jtraining
varargs
 
varkw
 
defaultsЪ
p 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
»B≈
!__inference_signature_wrapper_703input_1"Ф
Н≤Й
FullArgSpec
argsЪ 
varargs
 
varkwjkwargs
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
ѓ2ђ
G__inference_zero_padding2d_layer_call_and_return_conditional_losses_458а
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *@Ґ=
;К84€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€
Ф2С
,__inference_zero_padding2d_layer_call_fn_464а
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *@Ґ=
;К84€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€
®2•Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
®2•Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
µ2≤ѓ
¶≤Ґ
FullArgSpec$
argsЪ
jself
jx

jtraining
varargs
 
varkw
 
defaultsЪ
p 

kwonlyargsЪ 
kwonlydefaults™ 
annotations™ *
 
µ2≤ѓ
¶≤Ґ
FullArgSpec$
argsЪ
jself
jx

jtraining
varargs
 
varkw
 
defaultsЪ
p 

kwonlyargsЪ 
kwonlydefaults™ 
annotations™ *
 
®2•Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
®2•Ґ
Щ≤Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotations™ *
 
µ2≤ѓ
¶≤Ґ
FullArgSpec$
argsЪ
jself
jx

jtraining
varargs
 
varkw
 
defaultsЪ
p 

kwonlyargsЪ 
kwonlydefaults™ 
annotations™ *
 
µ2≤ѓ
¶≤Ґ
FullArgSpec$
argsЪ
jself
jx

jtraining
varargs
 
varkw
 
defaultsЪ
p 

kwonlyargsЪ 
kwonlydefaults™ 
annotations™ *
 Х
__inference__wrapped_model_451т%&'4Ґ1
*Ґ'
%К"
input_1€€€€€€€€€
™ "і™∞
,
flatten!К
flatten€€€€€€€€€ 
J
fused_conv2d_re_lu4К1
fused_conv2d_re_lu€€€€€€€€€
4
fused_dense%К"
fused_dense€€€€€€€€€§
@__inference_flatten_layer_call_and_return_conditional_losses_876`7Ґ4
-Ґ*
(К%
inputs€€€€€€€€€
™ "%Ґ"
К
0€€€€€€€€€ 
Ъ |
%__inference_flatten_layer_call_fn_881S7Ґ4
-Ґ*
(К%
inputs€€€€€€€€€
™ "К€€€€€€€€€ є
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_842j%6Ґ3
,Ґ)
#К 
x€€€€€€€€€
p
™ "-Ґ*
#К 
0€€€€€€€€€
Ъ є
K__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_856j%6Ґ3
,Ґ)
#К 
x€€€€€€€€€
p 
™ "-Ґ*
#К 
0€€€€€€€€€
Ъ С
0__inference_fused_conv2d_re_lu_layer_call_fn_863]%6Ґ3
,Ґ)
#К 
x€€€€€€€€€
p
™ " К€€€€€€€€€С
0__inference_fused_conv2d_re_lu_layer_call_fn_870]%6Ґ3
,Ґ)
#К 
x€€€€€€€€€
p 
™ " К€€€€€€€€€£
D__inference_fused_dense_layer_call_and_return_conditional_losses_895[&'.Ґ+
$Ґ!
К
x€€€€€€€€€ 
p
™ "%Ґ"
К
0€€€€€€€€€
Ъ £
D__inference_fused_dense_layer_call_and_return_conditional_losses_909[&'.Ґ+
$Ґ!
К
x€€€€€€€€€ 
p 
™ "%Ґ"
К
0€€€€€€€€€
Ъ {
)__inference_fused_dense_layer_call_fn_918N&'.Ґ+
$Ґ!
К
x€€€€€€€€€ 
p
™ "К€€€€€€€€€{
)__inference_fused_dense_layer_call_fn_927N&'.Ґ+
$Ґ!
К
x€€€€€€€€€ 
p 
™ "К€€€€€€€€€ъ
>__inference_model_layer_call_and_return_conditional_losses_607Ј%&'<Ґ9
2Ґ/
%К"
input_1€€€€€€€€€
p

 
™ "rҐo
hЪe
%К"
0/0€€€€€€€€€
К
0/1€€€€€€€€€ 
К
0/2€€€€€€€€€
Ъ ъ
>__inference_model_layer_call_and_return_conditional_losses_623Ј%&'<Ґ9
2Ґ/
%К"
input_1€€€€€€€€€
p 

 
™ "rҐo
hЪe
%К"
0/0€€€€€€€€€
К
0/1€€€€€€€€€ 
К
0/2€€€€€€€€€
Ъ щ
>__inference_model_layer_call_and_return_conditional_losses_741ґ%&';Ґ8
1Ґ.
$К!
inputs€€€€€€€€€
p

 
™ "rҐo
hЪe
%К"
0/0€€€€€€€€€
К
0/1€€€€€€€€€ 
К
0/2€€€€€€€€€
Ъ щ
>__inference_model_layer_call_and_return_conditional_losses_779ґ%&';Ґ8
1Ґ.
$К!
inputs€€€€€€€€€
p 

 
™ "rҐo
hЪe
%К"
0/0€€€€€€€€€
К
0/1€€€€€€€€€ 
К
0/2€€€€€€€€€
Ъ ѕ
#__inference_model_layer_call_fn_655І%&'<Ґ9
2Ґ/
%К"
input_1€€€€€€€€€
p

 
™ "bЪ_
#К 
0€€€€€€€€€
К
1€€€€€€€€€ 
К
2€€€€€€€€€ѕ
#__inference_model_layer_call_fn_686І%&'<Ґ9
2Ґ/
%К"
input_1€€€€€€€€€
p 

 
™ "bЪ_
#К 
0€€€€€€€€€
К
1€€€€€€€€€ 
К
2€€€€€€€€€ќ
#__inference_model_layer_call_fn_794¶%&';Ґ8
1Ґ.
$К!
inputs€€€€€€€€€
p

 
™ "bЪ_
#К 
0€€€€€€€€€
К
1€€€€€€€€€ 
К
2€€€€€€€€€ќ
#__inference_model_layer_call_fn_809¶%&';Ґ8
1Ґ.
$К!
inputs€€€€€€€€€
p 

 
™ "bЪ_
#К 
0€€€€€€€€€
К
1€€€€€€€€€ 
К
2€€€€€€€€€®
@__inference_reshape_layer_call_and_return_conditional_losses_823d3Ґ0
)Ґ&
$К!
inputs€€€€€€€€€
™ "-Ґ*
#К 
0€€€€€€€€€
Ъ А
%__inference_reshape_layer_call_fn_828W3Ґ0
)Ґ&
$К!
inputs€€€€€€€€€
™ " К€€€€€€€€€£
!__inference_signature_wrapper_703э%&'?Ґ<
Ґ 
5™2
0
input_1%К"
input_1€€€€€€€€€"і™∞
,
flatten!К
flatten€€€€€€€€€ 
J
fused_conv2d_re_lu4К1
fused_conv2d_re_lu€€€€€€€€€
4
fused_dense%К"
fused_dense€€€€€€€€€к
G__inference_zero_padding2d_layer_call_and_return_conditional_losses_458ЮRҐO
HҐE
CК@
inputs4€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€
™ "HҐE
>К;
04€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€
Ъ ¬
,__inference_zero_padding2d_layer_call_fn_464СRҐO
HҐE
CК@
inputs4€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€
™ ";К84€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€