��
��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
�
AvgPool

value"T
output"T"
ksize	list(int)(0"
strides	list(int)(0""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW"
Ttype:
2
8
Const
output"dtype"
valuetensor"
dtypetype
�
Conv2D

input"T
filter"T
output"T"
Ttype:	
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
�
MaxPool

input"T
output"T"
Ttype0:
2	"
ksize	list(int)(0"
strides	list(int)(0",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 ":
data_formatstringNHWC:
NHWCNCHWNCHW_VECT_C
:
Maximum
x"T
y"T
z"T"
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�
:
Minimum
x"T
y"T
z"T"
Ttype:

2	

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
_
Pad

input"T
paddings"	Tpaddings
output"T"	
Ttype"
	Tpaddingstype0:
2	
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.4.12v2.4.1-0-g85c8b2a817f8��

`
beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namebeta_1
Y
beta_1/Read/ReadVariableOpReadVariableOpbeta_1*
_output_shapes
: *
dtype0
`
beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namebeta_2
Y
beta_2/Read/ReadVariableOpReadVariableOpbeta_2*
_output_shapes
: *
dtype0
^
decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namedecay
W
decay/Read/ReadVariableOpReadVariableOpdecay*
_output_shapes
: *
dtype0
n
learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namelearning_rate
g
!learning_rate/Read/ReadVariableOpReadVariableOplearning_rate*
_output_shapes
: *
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
�
 fused_conv2d_re_lu/conv2d/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:<*1
shared_name" fused_conv2d_re_lu/conv2d/kernel
�
4fused_conv2d_re_lu/conv2d/kernel/Read/ReadVariableOpReadVariableOp fused_conv2d_re_lu/conv2d/kernel*&
_output_shapes
:<*
dtype0
�
+fused_max_pool_conv2d_re_lu/conv2d_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:<<*<
shared_name-+fused_max_pool_conv2d_re_lu/conv2d_1/kernel
�
?fused_max_pool_conv2d_re_lu/conv2d_1/kernel/Read/ReadVariableOpReadVariableOp+fused_max_pool_conv2d_re_lu/conv2d_1/kernel*&
_output_shapes
:<<*
dtype0
�
-fused_max_pool_conv2d_re_lu_1/conv2d_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:<8*>
shared_name/-fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel
�
Afused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/Read/ReadVariableOpReadVariableOp-fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel*&
_output_shapes
:<8*
dtype0
�
+fused_avg_pool_conv2d_re_lu/conv2d_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:8*<
shared_name-+fused_avg_pool_conv2d_re_lu/conv2d_3/kernel
�
?fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/Read/ReadVariableOpReadVariableOp+fused_avg_pool_conv2d_re_lu/conv2d_3/kernel*&
_output_shapes
:8*
dtype0
�
fused_dense/dense/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
*)
shared_namefused_dense/dense/kernel
�
,fused_dense/dense/kernel/Read/ReadVariableOpReadVariableOpfused_dense/dense/kernel*
_output_shapes
:	�
*
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
�
'Adam/fused_conv2d_re_lu/conv2d/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:<*8
shared_name)'Adam/fused_conv2d_re_lu/conv2d/kernel/m
�
;Adam/fused_conv2d_re_lu/conv2d/kernel/m/Read/ReadVariableOpReadVariableOp'Adam/fused_conv2d_re_lu/conv2d/kernel/m*&
_output_shapes
:<*
dtype0
�
2Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:<<*C
shared_name42Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/m
�
FAdam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/m/Read/ReadVariableOpReadVariableOp2Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/m*&
_output_shapes
:<<*
dtype0
�
4Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:<8*E
shared_name64Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/m
�
HAdam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/m/Read/ReadVariableOpReadVariableOp4Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/m*&
_output_shapes
:<8*
dtype0
�
2Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:8*C
shared_name42Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/m
�
FAdam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/m/Read/ReadVariableOpReadVariableOp2Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/m*&
_output_shapes
:8*
dtype0
�
Adam/fused_dense/dense/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
*0
shared_name!Adam/fused_dense/dense/kernel/m
�
3Adam/fused_dense/dense/kernel/m/Read/ReadVariableOpReadVariableOpAdam/fused_dense/dense/kernel/m*
_output_shapes
:	�
*
dtype0
�
'Adam/fused_conv2d_re_lu/conv2d/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:<*8
shared_name)'Adam/fused_conv2d_re_lu/conv2d/kernel/v
�
;Adam/fused_conv2d_re_lu/conv2d/kernel/v/Read/ReadVariableOpReadVariableOp'Adam/fused_conv2d_re_lu/conv2d/kernel/v*&
_output_shapes
:<*
dtype0
�
2Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:<<*C
shared_name42Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/v
�
FAdam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/v/Read/ReadVariableOpReadVariableOp2Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/v*&
_output_shapes
:<<*
dtype0
�
4Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:<8*E
shared_name64Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/v
�
HAdam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/v/Read/ReadVariableOpReadVariableOp4Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/v*&
_output_shapes
:<8*
dtype0
�
2Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:8*C
shared_name42Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/v
�
FAdam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/v/Read/ReadVariableOpReadVariableOp2Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/v*&
_output_shapes
:8*
dtype0
�
Adam/fused_dense/dense/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
*0
shared_name!Adam/fused_dense/dense/kernel/v
�
3Adam/fused_dense/dense/kernel/v/Read/ReadVariableOpReadVariableOpAdam/fused_dense/dense/kernel/v*
_output_shapes
:	�
*
dtype0

NoOpNoOp
�^
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�]
value�]B�] B�]
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer_with_weights-2
layer-2
layer_with_weights-3
layer-3
layer-4
layer_with_weights-4
layer-5
	optimizer
	variables
	regularization_losses

trainable_variables
	keras_api

signatures
�
zeropadding

conv2d
quantize_pool

clamp_pool
quantize
	clamp
	variables
regularization_losses
trainable_variables
	keras_api
�
pool
zeropadding

conv2d
quantize_pool

clamp_pool
quantize
	clamp
	variables
regularization_losses
 trainable_variables
!	keras_api
�
"pool
#zeropadding

$conv2d
%quantize_pool
&
clamp_pool
'quantize
	(clamp
)	variables
*regularization_losses
+trainable_variables
,	keras_api
�
-pool
.zeropadding

/conv2d
0quantize_pool
1
clamp_pool
2quantize
	3clamp
4	variables
5regularization_losses
6trainable_variables
7	keras_api
R
8	variables
9regularization_losses
:trainable_variables
;	keras_api
v
	<Dense
=quantize
	>clamp
?	variables
@regularization_losses
Atrainable_variables
B	keras_api
�

Cbeta_1

Dbeta_2
	Edecay
Flearning_rate
GiterHm�Im�Jm�Km�Lm�Hv�Iv�Jv�Kv�Lv�
#
H0
I1
J2
K3
L4
 
#
H0
I1
J2
K3
L4
�

Mlayers
Nnon_trainable_variables
	variables
	regularization_losses
Ometrics
Player_regularization_losses
Qlayer_metrics

trainable_variables
 
R
R	variables
Sregularization_losses
Ttrainable_variables
U	keras_api
^

Hkernel
V	variables
Wregularization_losses
Xtrainable_variables
Y	keras_api

Z	keras_api

[	keras_api

\	keras_api
R
]	variables
^regularization_losses
_trainable_variables
`	keras_api

H0
 

H0
�
anon_trainable_variables
	variables
regularization_losses
blayer_metrics
cmetrics
dlayer_regularization_losses

elayers
trainable_variables
R
f	variables
gregularization_losses
htrainable_variables
i	keras_api
R
j	variables
kregularization_losses
ltrainable_variables
m	keras_api
^

Ikernel
n	variables
oregularization_losses
ptrainable_variables
q	keras_api

r	keras_api
R
s	variables
tregularization_losses
utrainable_variables
v	keras_api

w	keras_api
R
x	variables
yregularization_losses
ztrainable_variables
{	keras_api

I0
 

I0
�
|non_trainable_variables
	variables
regularization_losses
}layer_metrics
~metrics
layer_regularization_losses
�layers
 trainable_variables
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api
b

Jkernel
�	variables
�regularization_losses
�trainable_variables
�	keras_api

�	keras_api
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api

�	keras_api
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api

J0
 

J0
�
�non_trainable_variables
)	variables
*regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
+trainable_variables
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api
b

Kkernel
�	variables
�regularization_losses
�trainable_variables
�	keras_api

�	keras_api
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api

�	keras_api
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api

K0
 

K0
�
�non_trainable_variables
4	variables
5regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
6trainable_variables
 
 
 
�
�non_trainable_variables
8	variables
9regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
:trainable_variables
b

Lkernel
�	variables
�regularization_losses
�trainable_variables
�	keras_api

�	keras_api
V
�	variables
�regularization_losses
�trainable_variables
�	keras_api

L0
 

L0
�
�non_trainable_variables
?	variables
@regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
Atrainable_variables
GE
VARIABLE_VALUEbeta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
GE
VARIABLE_VALUEbeta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
EC
VARIABLE_VALUEdecay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUElearning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
\Z
VARIABLE_VALUE fused_conv2d_re_lu/conv2d/kernel&variables/0/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUE+fused_max_pool_conv2d_re_lu/conv2d_1/kernel&variables/1/.ATTRIBUTES/VARIABLE_VALUE
ig
VARIABLE_VALUE-fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel&variables/2/.ATTRIBUTES/VARIABLE_VALUE
ge
VARIABLE_VALUE+fused_avg_pool_conv2d_re_lu/conv2d_3/kernel&variables/3/.ATTRIBUTES/VARIABLE_VALUE
TR
VARIABLE_VALUEfused_dense/dense/kernel&variables/4/.ATTRIBUTES/VARIABLE_VALUE
*
0
1
2
3
4
5
 

�0
�1
 
 
 
 
 
�
�non_trainable_variables
R	variables
Sregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
Ttrainable_variables

H0
 

H0
�
�non_trainable_variables
V	variables
Wregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
Xtrainable_variables
 
 
 
 
 
 
�
�non_trainable_variables
]	variables
^regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
_trainable_variables
 
 
 
 
*
0
1
2
3
4
5
 
 
 
�
�non_trainable_variables
f	variables
gregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
htrainable_variables
 
 
 
�
�non_trainable_variables
j	variables
kregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
ltrainable_variables

I0
 

I0
�
�non_trainable_variables
n	variables
oregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
ptrainable_variables
 
 
 
 
�
�non_trainable_variables
s	variables
tregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
utrainable_variables
 
 
 
 
�
�non_trainable_variables
x	variables
yregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
ztrainable_variables
 
 
 
 
1
0
1
2
3
4
5
6
 
 
 
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
 
 
 
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables

J0
 

J0
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
 
 
 
 
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
 
 
 
 
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
 
 
 
 
1
"0
#1
$2
%3
&4
'5
(6
 
 
 
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
 
 
 
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables

K0
 

K0
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
 
 
 
 
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
 
 
 
 
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
 
 
 
 
1
-0
.1
/2
03
14
25
36
 
 
 
 
 

L0
 

L0
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
 
 
 
 
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
 
 
 
 

<0
=1
>2
8

�total

�count
�	variables
�	keras_api
I

�total

�count
�
_fn_kwargs
�	variables
�	keras_api
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1

�	variables
}
VARIABLE_VALUE'Adam/fused_conv2d_re_lu/conv2d/kernel/mBvariables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE2Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/mBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE4Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/mBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE2Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/mBvariables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
wu
VARIABLE_VALUEAdam/fused_dense/dense/kernel/mBvariables/4/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}
VARIABLE_VALUE'Adam/fused_conv2d_re_lu/conv2d/kernel/vBvariables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE2Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/vBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE4Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/vBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE2Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/vBvariables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
wu
VARIABLE_VALUEAdam/fused_dense/dense/kernel/vBvariables/4/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
serving_default_input_1Placeholder*/
_output_shapes
:���������  *
dtype0*$
shape:���������  
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1 fused_conv2d_re_lu/conv2d/kernel+fused_max_pool_conv2d_re_lu/conv2d_1/kernel-fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel+fused_avg_pool_conv2d_re_lu/conv2d_3/kernelfused_dense/dense/kernel*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
*0
config_proto 

CPU

GPU2*0J 8� *-
f(R&
$__inference_signature_wrapper_182523
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filenamebeta_1/Read/ReadVariableOpbeta_2/Read/ReadVariableOpdecay/Read/ReadVariableOp!learning_rate/Read/ReadVariableOpAdam/iter/Read/ReadVariableOp4fused_conv2d_re_lu/conv2d/kernel/Read/ReadVariableOp?fused_max_pool_conv2d_re_lu/conv2d_1/kernel/Read/ReadVariableOpAfused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/Read/ReadVariableOp?fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/Read/ReadVariableOp,fused_dense/dense/kernel/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp;Adam/fused_conv2d_re_lu/conv2d/kernel/m/Read/ReadVariableOpFAdam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/m/Read/ReadVariableOpHAdam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/m/Read/ReadVariableOpFAdam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/m/Read/ReadVariableOp3Adam/fused_dense/dense/kernel/m/Read/ReadVariableOp;Adam/fused_conv2d_re_lu/conv2d/kernel/v/Read/ReadVariableOpFAdam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/v/Read/ReadVariableOpHAdam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/v/Read/ReadVariableOpFAdam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/v/Read/ReadVariableOp3Adam/fused_dense/dense/kernel/v/Read/ReadVariableOpConst*%
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *(
f#R!
__inference__traced_save_182997
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamebeta_1beta_2decaylearning_rate	Adam/iter fused_conv2d_re_lu/conv2d/kernel+fused_max_pool_conv2d_re_lu/conv2d_1/kernel-fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel+fused_avg_pool_conv2d_re_lu/conv2d_3/kernelfused_dense/dense/kerneltotalcounttotal_1count_1'Adam/fused_conv2d_re_lu/conv2d/kernel/m2Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/m4Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/m2Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/mAdam/fused_dense/dense/kernel/m'Adam/fused_conv2d_re_lu/conv2d/kernel/v2Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/v4Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/v2Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/vAdam/fused_dense/dense/kernel/v*$
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *+
f&R$
"__inference__traced_restore_183079��	
�
e
I__inference_max_pooling2d_layer_call_and_return_conditional_losses_182076

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
2	
MaxPool�
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
_
C__inference_flatten_layer_call_and_return_conditional_losses_182357

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"�����   2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:����������2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������:W S
/
_output_shapes
:���������
 
_user_specified_nameinputs
�
M
1__inference_zero_padding2d_3_layer_call_fn_182145

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *U
fPRN
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_1821392
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_182260
x+
'conv2d_2_conv2d_readvariableop_resource
identity��conv2d_2/Conv2D/ReadVariableOp�
max_pooling2d_1/MaxPoolMaxPoolx*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
2
max_pooling2d_1/MaxPool�
zero_padding2d_2/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_2/Pad/paddings�
zero_padding2d_2/PadPad max_pooling2d_1/MaxPool:output:0&zero_padding2d_2/Pad/paddings:output:0*
T0*/
_output_shapes
:���������

<2
zero_padding2d_2/Pad�
conv2d_2/Conv2D/ReadVariableOpReadVariableOp'conv2d_2_conv2d_readvariableop_resource*&
_output_shapes
:<8*
dtype02 
conv2d_2/Conv2D/ReadVariableOp�
conv2d_2/Conv2DConv2Dzero_padding2d_2/Pad:output:0&conv2d_2/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������8*
paddingVALID*
strides
2
conv2d_2/Conv2Dz
conv2d_2/ReluReluconv2d_2/Conv2D:output:0*
T0*/
_output_shapes
:���������82
conv2d_2/Relu�
clamp_2/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_2/clip_by_value/Minimum/y�
clamp_2/clip_by_value/MinimumMinimumconv2d_2/Relu:activations:0(clamp_2/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82
clamp_2/clip_by_value/Minimumw
clamp_2/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_2/clip_by_value/y�
clamp_2/clip_by_valueMaximum!clamp_2/clip_by_value/Minimum:z:0 clamp_2/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������82
clamp_2/clip_by_value�
IdentityIdentityclamp_2/clip_by_value:z:0^conv2d_2/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������82

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������<:2@
conv2d_2/Conv2D/ReadVariableOpconv2d_2/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������<

_user_specified_namex
�
�
$__inference_signature_wrapper_182523
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
*0
config_proto 

CPU

GPU2*0J 8� **
f%R#
!__inference__wrapped_model_1820572
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::22
StatefulPartitionedCallStatefulPartitionedCall:X T
/
_output_shapes
:���������  
!
_user_specified_name	input_1
�"
�
F__inference_sequential_layer_call_and_return_conditional_losses_182407
input_1
fused_conv2d_re_lu_182193&
"fused_max_pool_conv2d_re_lu_182242(
$fused_max_pool_conv2d_re_lu_1_182291&
"fused_avg_pool_conv2d_re_lu_182348
fused_dense_182403
identity��3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall�*fused_conv2d_re_lu/StatefulPartitionedCall�#fused_dense/StatefulPartitionedCall�3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall�5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall�
*fused_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCallinput_1fused_conv2d_re_lu_182193*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������  <*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_1821632,
*fused_conv2d_re_lu/StatefulPartitionedCall�
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall3fused_conv2d_re_lu/StatefulPartitionedCall:output:0"fused_max_pool_conv2d_re_lu_182242*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������<*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_18221125
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall�
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCallStatefulPartitionedCall<fused_max_pool_conv2d_re_lu/StatefulPartitionedCall:output:0$fused_max_pool_conv2d_re_lu_1_182291*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������8*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *b
f]R[
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_18226027
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall�
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall>fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall:output:0"fused_avg_pool_conv2d_re_lu_182348*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_18231325
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall�
flatten/PartitionedCallPartitionedCall<fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_flatten_layer_call_and_return_conditional_losses_1823572
flatten/PartitionedCall�
#fused_dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0fused_dense_182403*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_fused_dense_layer_call_and_return_conditional_losses_1823762%
#fused_dense/StatefulPartitionedCall�
IdentityIdentity,fused_dense/StatefulPartitionedCall:output:04^fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall4^fused_max_pool_conv2d_re_lu/StatefulPartitionedCall6^fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::2j
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall2X
*fused_conv2d_re_lu/StatefulPartitionedCall*fused_conv2d_re_lu/StatefulPartitionedCall2J
#fused_dense/StatefulPartitionedCall#fused_dense/StatefulPartitionedCall2j
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall2n
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall:X T
/
_output_shapes
:���������  
!
_user_specified_name	input_1
�
}
<__inference_fused_max_pool_conv2d_re_lu_layer_call_fn_182759
x
unknown
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������<*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_1822262
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:���������<2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  <:22
StatefulPartitionedCallStatefulPartitionedCall:R N
/
_output_shapes
:���������  <

_user_specified_namex
�

>__inference_fused_max_pool_conv2d_re_lu_1_layer_call_fn_182796
x
unknown
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������8*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *b
f]R[
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_1822752
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:���������82

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������<:22
StatefulPartitionedCallStatefulPartitionedCall:R N
/
_output_shapes
:���������<

_user_specified_namex
�
�
G__inference_fused_dense_layer_call_and_return_conditional_losses_182877
x(
$dense_matmul_readvariableop_resource
identity��dense/MatMul/ReadVariableOp�
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02
dense/MatMul/ReadVariableOp�
dense/MatMulMatMulx#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense/MatMul�
clamp_5/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2!
clamp_5/clip_by_value/Minimum/y�
clamp_5/clip_by_value/MinimumMinimumdense/MatMul:product:0(clamp_5/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:���������
2
clamp_5/clip_by_value/Minimumw
clamp_5/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   �2
clamp_5/clip_by_value/y�
clamp_5/clip_by_valueMaximum!clamp_5/clip_by_value/Minimum:z:0 clamp_5/clip_by_value/y:output:0*
T0*'
_output_shapes
:���������
2
clamp_5/clip_by_value�
IdentityIdentityclamp_5/clip_by_value:z:0^dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*+
_input_shapes
:����������:2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp:K G
(
_output_shapes
:����������

_user_specified_namex
�
i
M__inference_average_pooling2d_layer_call_and_return_conditional_losses_182126

inputs
identity�
AvgPoolAvgPoolinputs*
T0*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
2	
AvgPool�
IdentityIdentityAvgPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�i
�
"__inference__traced_restore_183079
file_prefix
assignvariableop_beta_1
assignvariableop_1_beta_2
assignvariableop_2_decay$
 assignvariableop_3_learning_rate 
assignvariableop_4_adam_iter7
3assignvariableop_5_fused_conv2d_re_lu_conv2d_kernelB
>assignvariableop_6_fused_max_pool_conv2d_re_lu_conv2d_1_kernelD
@assignvariableop_7_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernelB
>assignvariableop_8_fused_avg_pool_conv2d_re_lu_conv2d_3_kernel/
+assignvariableop_9_fused_dense_dense_kernel
assignvariableop_10_total
assignvariableop_11_count
assignvariableop_12_total_1
assignvariableop_13_count_1?
;assignvariableop_14_adam_fused_conv2d_re_lu_conv2d_kernel_mJ
Fassignvariableop_15_adam_fused_max_pool_conv2d_re_lu_conv2d_1_kernel_mL
Hassignvariableop_16_adam_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernel_mJ
Fassignvariableop_17_adam_fused_avg_pool_conv2d_re_lu_conv2d_3_kernel_m7
3assignvariableop_18_adam_fused_dense_dense_kernel_m?
;assignvariableop_19_adam_fused_conv2d_re_lu_conv2d_kernel_vJ
Fassignvariableop_20_adam_fused_max_pool_conv2d_re_lu_conv2d_1_kernel_vL
Hassignvariableop_21_adam_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernel_vJ
Fassignvariableop_22_adam_fused_avg_pool_conv2d_re_lu_conv2d_3_kernel_v7
3assignvariableop_23_adam_fused_dense_dense_kernel_v
identity_25��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�

value�
B�
B+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB&variables/0/.ATTRIBUTES/VARIABLE_VALUEB&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEB&variables/4/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/4/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/4/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*x
_output_shapesf
d:::::::::::::::::::::::::*'
dtypes
2	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOpassignvariableop_beta_1Identity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOpassignvariableop_1_beta_2Identity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOpassignvariableop_2_decayIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp assignvariableop_3_learning_rateIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0	*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOpassignvariableop_4_adam_iterIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp3assignvariableop_5_fused_conv2d_re_lu_conv2d_kernelIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp>assignvariableop_6_fused_max_pool_conv2d_re_lu_conv2d_1_kernelIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp@assignvariableop_7_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernelIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp>assignvariableop_8_fused_avg_pool_conv2d_re_lu_conv2d_3_kernelIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOp+assignvariableop_9_fused_dense_dense_kernelIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOpassignvariableop_10_totalIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOpassignvariableop_11_countIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOpassignvariableop_12_total_1Identity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOpassignvariableop_13_count_1Identity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp;assignvariableop_14_adam_fused_conv2d_re_lu_conv2d_kernel_mIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOpFassignvariableop_15_adam_fused_max_pool_conv2d_re_lu_conv2d_1_kernel_mIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOpHassignvariableop_16_adam_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernel_mIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOpFassignvariableop_17_adam_fused_avg_pool_conv2d_re_lu_conv2d_3_kernel_mIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOp3assignvariableop_18_adam_fused_dense_dense_kernel_mIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOp;assignvariableop_19_adam_fused_conv2d_re_lu_conv2d_kernel_vIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOpFassignvariableop_20_adam_fused_max_pool_conv2d_re_lu_conv2d_1_kernel_vIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOpHassignvariableop_21_adam_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernel_vIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOpFassignvariableop_22_adam_fused_avg_pool_conv2d_re_lu_conv2d_3_kernel_vIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOp3assignvariableop_23_adam_fused_dense_dense_kernel_vIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_239
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_24Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_24�
Identity_25IdentityIdentity_24:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*
T0*
_output_shapes
: 2
Identity_25"#
identity_25Identity_25:output:0*u
_input_shapesd
b: ::::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�
�
+__inference_sequential_layer_call_fn_182658

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
*0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_1824502
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::22
StatefulPartitionedCallStatefulPartitionedCall:W S
/
_output_shapes
:���������  
 
_user_specified_nameinputs
�
�
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182332
x+
'conv2d_3_conv2d_readvariableop_resource
identity��conv2d_3/Conv2D/ReadVariableOp�
average_pooling2d/AvgPoolAvgPoolx*
T0*/
_output_shapes
:���������8*
ksize
*
paddingVALID*
strides
2
average_pooling2d/AvgPool�
clamp_3/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_3/clip_by_value/Minimum/y�
clamp_3/clip_by_value/MinimumMinimum"average_pooling2d/AvgPool:output:0(clamp_3/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82
clamp_3/clip_by_value/Minimumw
clamp_3/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_3/clip_by_value/y�
clamp_3/clip_by_valueMaximum!clamp_3/clip_by_value/Minimum:z:0 clamp_3/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������82
clamp_3/clip_by_value�
zero_padding2d_3/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_3/Pad/paddings�
zero_padding2d_3/PadPadclamp_3/clip_by_value:z:0&zero_padding2d_3/Pad/paddings:output:0*
T0*/
_output_shapes
:���������82
zero_padding2d_3/Pad�
conv2d_3/Conv2D/ReadVariableOpReadVariableOp'conv2d_3_conv2d_readvariableop_resource*&
_output_shapes
:8*
dtype02 
conv2d_3/Conv2D/ReadVariableOp�
conv2d_3/Conv2DConv2Dzero_padding2d_3/Pad:output:0&conv2d_3/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2
conv2d_3/Conv2Dz
conv2d_3/ReluReluconv2d_3/Conv2D:output:0*
T0*/
_output_shapes
:���������2
conv2d_3/Relu�
clamp_4/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_4/clip_by_value/Minimum/y�
clamp_4/clip_by_value/MinimumMinimumconv2d_3/Relu:activations:0(clamp_4/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������2
clamp_4/clip_by_value/Minimumw
clamp_4/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_4/clip_by_value/y�
clamp_4/clip_by_valueMaximum!clamp_4/clip_by_value/Minimum:z:0 clamp_4/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������2
clamp_4/clip_by_value�
IdentityIdentityclamp_4/clip_by_value:z:0^conv2d_3/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������8:2@
conv2d_3/Conv2D/ReadVariableOpconv2d_3/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������8

_user_specified_namex
�
_
C__inference_flatten_layer_call_and_return_conditional_losses_182861

inputs
identity_
ConstConst*
_output_shapes
:*
dtype0*
valueB"�����   2
Consth
ReshapeReshapeinputsConst:output:0*
T0*(
_output_shapes
:����������2	
Reshapee
IdentityIdentityReshape:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������:W S
/
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_182687
x)
%conv2d_conv2d_readvariableop_resource
identity��conv2d/Conv2D/ReadVariableOp�
zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d/Pad/paddings�
zero_padding2d/PadPadx$zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:���������""2
zero_padding2d/Pad�
conv2d/Conv2D/ReadVariableOpReadVariableOp%conv2d_conv2d_readvariableop_resource*&
_output_shapes
:<*
dtype02
conv2d/Conv2D/ReadVariableOp�
conv2d/Conv2DConv2Dzero_padding2d/Pad:output:0$conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������  <*
paddingVALID*
strides
2
conv2d/Conv2Dt
conv2d/ReluReluconv2d/Conv2D:output:0*
T0*/
_output_shapes
:���������  <2
conv2d/Relu�
clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
clamp/clip_by_value/Minimum/y�
clamp/clip_by_value/MinimumMinimumconv2d/Relu:activations:0&clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������  <2
clamp/clip_by_value/Minimums
clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp/clip_by_value/y�
clamp/clip_by_valueMaximumclamp/clip_by_value/Minimum:z:0clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������  <2
clamp/clip_by_value�
IdentityIdentityclamp/clip_by_value:z:0^conv2d/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������  <2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  :2<
conv2d/Conv2D/ReadVariableOpconv2d/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������  

_user_specified_namex
�
�
+__inference_sequential_layer_call_fn_182673

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
*0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_1824852
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::22
StatefulPartitionedCallStatefulPartitionedCall:W S
/
_output_shapes
:���������  
 
_user_specified_nameinputs
�
�
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_182163
x)
%conv2d_conv2d_readvariableop_resource
identity��conv2d/Conv2D/ReadVariableOp�
zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d/Pad/paddings�
zero_padding2d/PadPadx$zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:���������""2
zero_padding2d/Pad�
conv2d/Conv2D/ReadVariableOpReadVariableOp%conv2d_conv2d_readvariableop_resource*&
_output_shapes
:<*
dtype02
conv2d/Conv2D/ReadVariableOp�
conv2d/Conv2DConv2Dzero_padding2d/Pad:output:0$conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������  <*
paddingVALID*
strides
2
conv2d/Conv2Dt
conv2d/ReluReluconv2d/Conv2D:output:0*
T0*/
_output_shapes
:���������  <2
conv2d/Relu�
clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
clamp/clip_by_value/Minimum/y�
clamp/clip_by_value/MinimumMinimumconv2d/Relu:activations:0&clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������  <2
clamp/clip_by_value/Minimums
clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp/clip_by_value/y�
clamp/clip_by_valueMaximumclamp/clip_by_value/Minimum:z:0clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������  <2
clamp/clip_by_value�
IdentityIdentityclamp/clip_by_value:z:0^conv2d/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������  <2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  :2<
conv2d/Conv2D/ReadVariableOpconv2d/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������  

_user_specified_namex
�
}
<__inference_fused_max_pool_conv2d_re_lu_layer_call_fn_182752
x
unknown
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������<*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_1822262
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:���������<2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  <:22
StatefulPartitionedCallStatefulPartitionedCall:R N
/
_output_shapes
:���������  <

_user_specified_namex
�
�
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_182275
x+
'conv2d_2_conv2d_readvariableop_resource
identity��conv2d_2/Conv2D/ReadVariableOp�
max_pooling2d_1/MaxPoolMaxPoolx*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
2
max_pooling2d_1/MaxPool�
zero_padding2d_2/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_2/Pad/paddings�
zero_padding2d_2/PadPad max_pooling2d_1/MaxPool:output:0&zero_padding2d_2/Pad/paddings:output:0*
T0*/
_output_shapes
:���������

<2
zero_padding2d_2/Pad�
conv2d_2/Conv2D/ReadVariableOpReadVariableOp'conv2d_2_conv2d_readvariableop_resource*&
_output_shapes
:<8*
dtype02 
conv2d_2/Conv2D/ReadVariableOp�
conv2d_2/Conv2DConv2Dzero_padding2d_2/Pad:output:0&conv2d_2/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������8*
paddingVALID*
strides
2
conv2d_2/Conv2Dz
conv2d_2/ReluReluconv2d_2/Conv2D:output:0*
T0*/
_output_shapes
:���������82
conv2d_2/Relu�
clamp_2/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_2/clip_by_value/Minimum/y�
clamp_2/clip_by_value/MinimumMinimumconv2d_2/Relu:activations:0(clamp_2/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82
clamp_2/clip_by_value/Minimumw
clamp_2/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_2/clip_by_value/y�
clamp_2/clip_by_valueMaximum!clamp_2/clip_by_value/Minimum:z:0 clamp_2/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������82
clamp_2/clip_by_value�
IdentityIdentityclamp_2/clip_by_value:z:0^conv2d_2/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������82

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������<:2@
conv2d_2/Conv2D/ReadVariableOpconv2d_2/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������<

_user_specified_namex
�
t
3__inference_fused_conv2d_re_lu_layer_call_fn_182715
x
unknown
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������  <*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_1821772
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:���������  <2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  :22
StatefulPartitionedCallStatefulPartitionedCall:R N
/
_output_shapes
:���������  

_user_specified_namex
�"
�
F__inference_sequential_layer_call_and_return_conditional_losses_182485

inputs
fused_conv2d_re_lu_182468&
"fused_max_pool_conv2d_re_lu_182471(
$fused_max_pool_conv2d_re_lu_1_182474&
"fused_avg_pool_conv2d_re_lu_182477
fused_dense_182481
identity��3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall�*fused_conv2d_re_lu/StatefulPartitionedCall�#fused_dense/StatefulPartitionedCall�3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall�5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall�
*fused_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCallinputsfused_conv2d_re_lu_182468*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������  <*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_1821772,
*fused_conv2d_re_lu/StatefulPartitionedCall�
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall3fused_conv2d_re_lu/StatefulPartitionedCall:output:0"fused_max_pool_conv2d_re_lu_182471*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������<*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_18222625
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall�
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCallStatefulPartitionedCall<fused_max_pool_conv2d_re_lu/StatefulPartitionedCall:output:0$fused_max_pool_conv2d_re_lu_1_182474*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������8*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *b
f]R[
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_18227527
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall�
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall>fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall:output:0"fused_avg_pool_conv2d_re_lu_182477*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_18233225
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall�
flatten/PartitionedCallPartitionedCall<fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_flatten_layer_call_and_return_conditional_losses_1823572
flatten/PartitionedCall�
#fused_dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0fused_dense_182481*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_fused_dense_layer_call_and_return_conditional_losses_1823872%
#fused_dense/StatefulPartitionedCall�
IdentityIdentity,fused_dense/StatefulPartitionedCall:output:04^fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall4^fused_max_pool_conv2d_re_lu/StatefulPartitionedCall6^fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::2j
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall2X
*fused_conv2d_re_lu/StatefulPartitionedCall*fused_conv2d_re_lu/StatefulPartitionedCall2J
#fused_dense/StatefulPartitionedCall#fused_dense/StatefulPartitionedCall2j
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall2n
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall:W S
/
_output_shapes
:���������  
 
_user_specified_nameinputs
�
�
+__inference_sequential_layer_call_fn_182463
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
*0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_1824502
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::22
StatefulPartitionedCallStatefulPartitionedCall:X T
/
_output_shapes
:���������  
!
_user_specified_name	input_1
�
m
,__inference_fused_dense_layer_call_fn_182902
x
unknown
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_fused_dense_layer_call_and_return_conditional_losses_1823872
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*+
_input_shapes
:����������:22
StatefulPartitionedCallStatefulPartitionedCall:K G
(
_output_shapes
:����������

_user_specified_namex
�
�
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182226
x+
'conv2d_1_conv2d_readvariableop_resource
identity��conv2d_1/Conv2D/ReadVariableOp�
max_pooling2d/MaxPoolMaxPoolx*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
2
max_pooling2d/MaxPool�
zero_padding2d_1/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_1/Pad/paddings�
zero_padding2d_1/PadPadmax_pooling2d/MaxPool:output:0&zero_padding2d_1/Pad/paddings:output:0*
T0*/
_output_shapes
:���������<2
zero_padding2d_1/Pad�
conv2d_1/Conv2D/ReadVariableOpReadVariableOp'conv2d_1_conv2d_readvariableop_resource*&
_output_shapes
:<<*
dtype02 
conv2d_1/Conv2D/ReadVariableOp�
conv2d_1/Conv2DConv2Dzero_padding2d_1/Pad:output:0&conv2d_1/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<*
paddingVALID*
strides
2
conv2d_1/Conv2Dz
conv2d_1/ReluReluconv2d_1/Conv2D:output:0*
T0*/
_output_shapes
:���������<2
conv2d_1/Relu�
clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_1/clip_by_value/Minimum/y�
clamp_1/clip_by_value/MinimumMinimumconv2d_1/Relu:activations:0(clamp_1/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������<2
clamp_1/clip_by_value/Minimumw
clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_1/clip_by_value/y�
clamp_1/clip_by_valueMaximum!clamp_1/clip_by_value/Minimum:z:0 clamp_1/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������<2
clamp_1/clip_by_value�
IdentityIdentityclamp_1/clip_by_value:z:0^conv2d_1/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������<2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  <:2@
conv2d_1/Conv2D/ReadVariableOpconv2d_1/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������  <

_user_specified_namex
�
N
2__inference_average_pooling2d_layer_call_fn_182132

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *V
fQRO
M__inference_average_pooling2d_layer_call_and_return_conditional_losses_1821262
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182730
x+
'conv2d_1_conv2d_readvariableop_resource
identity��conv2d_1/Conv2D/ReadVariableOp�
max_pooling2d/MaxPoolMaxPoolx*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
2
max_pooling2d/MaxPool�
zero_padding2d_1/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_1/Pad/paddings�
zero_padding2d_1/PadPadmax_pooling2d/MaxPool:output:0&zero_padding2d_1/Pad/paddings:output:0*
T0*/
_output_shapes
:���������<2
zero_padding2d_1/Pad�
conv2d_1/Conv2D/ReadVariableOpReadVariableOp'conv2d_1_conv2d_readvariableop_resource*&
_output_shapes
:<<*
dtype02 
conv2d_1/Conv2D/ReadVariableOp�
conv2d_1/Conv2DConv2Dzero_padding2d_1/Pad:output:0&conv2d_1/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<*
paddingVALID*
strides
2
conv2d_1/Conv2Dz
conv2d_1/ReluReluconv2d_1/Conv2D:output:0*
T0*/
_output_shapes
:���������<2
conv2d_1/Relu�
clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_1/clip_by_value/Minimum/y�
clamp_1/clip_by_value/MinimumMinimumconv2d_1/Relu:activations:0(clamp_1/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������<2
clamp_1/clip_by_value/Minimumw
clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_1/clip_by_value/y�
clamp_1/clip_by_valueMaximum!clamp_1/clip_by_value/Minimum:z:0 clamp_1/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������<2
clamp_1/clip_by_value�
IdentityIdentityclamp_1/clip_by_value:z:0^conv2d_1/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������<2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  <:2@
conv2d_1/Conv2D/ReadVariableOpconv2d_1/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������  <

_user_specified_namex
�
h
L__inference_zero_padding2d_1_layer_call_and_return_conditional_losses_182089

inputs
identity�
Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
Pad/paddings�
PadPadinputsPad/paddings:output:0*
T0*J
_output_shapes8
6:4������������������������������������2
Pad�
IdentityIdentityPad:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
}
<__inference_fused_avg_pool_conv2d_re_lu_layer_call_fn_182848
x
unknown
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_1823322
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������8:22
StatefulPartitionedCallStatefulPartitionedCall:R N
/
_output_shapes
:���������8

_user_specified_namex
�
h
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_182114

inputs
identity�
Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
Pad/paddings�
PadPadinputsPad/paddings:output:0*
T0*J
_output_shapes8
6:4������������������������������������2
Pad�
IdentityIdentityPad:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
G__inference_fused_dense_layer_call_and_return_conditional_losses_182387
x(
$dense_matmul_readvariableop_resource
identity��dense/MatMul/ReadVariableOp�
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02
dense/MatMul/ReadVariableOp�
dense/MatMulMatMulx#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense/MatMul�
clamp_5/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2!
clamp_5/clip_by_value/Minimum/y�
clamp_5/clip_by_value/MinimumMinimumdense/MatMul:product:0(clamp_5/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:���������
2
clamp_5/clip_by_value/Minimumw
clamp_5/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   �2
clamp_5/clip_by_value/y�
clamp_5/clip_by_valueMaximum!clamp_5/clip_by_value/Minimum:z:0 clamp_5/clip_by_value/y:output:0*
T0*'
_output_shapes
:���������
2
clamp_5/clip_by_value�
IdentityIdentityclamp_5/clip_by_value:z:0^dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*+
_input_shapes
:����������:2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp:K G
(
_output_shapes
:����������

_user_specified_namex
�
�
+__inference_sequential_layer_call_fn_182498
input_1
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1unknown	unknown_0	unknown_1	unknown_2	unknown_3*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
*0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_sequential_layer_call_and_return_conditional_losses_1824852
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::22
StatefulPartitionedCallStatefulPartitionedCall:X T
/
_output_shapes
:���������  
!
_user_specified_name	input_1
�"
�
F__inference_sequential_layer_call_and_return_conditional_losses_182427
input_1
fused_conv2d_re_lu_182410&
"fused_max_pool_conv2d_re_lu_182413(
$fused_max_pool_conv2d_re_lu_1_182416&
"fused_avg_pool_conv2d_re_lu_182419
fused_dense_182423
identity��3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall�*fused_conv2d_re_lu/StatefulPartitionedCall�#fused_dense/StatefulPartitionedCall�3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall�5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall�
*fused_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCallinput_1fused_conv2d_re_lu_182410*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������  <*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_1821772,
*fused_conv2d_re_lu/StatefulPartitionedCall�
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall3fused_conv2d_re_lu/StatefulPartitionedCall:output:0"fused_max_pool_conv2d_re_lu_182413*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������<*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_18222625
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall�
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCallStatefulPartitionedCall<fused_max_pool_conv2d_re_lu/StatefulPartitionedCall:output:0$fused_max_pool_conv2d_re_lu_1_182416*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������8*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *b
f]R[
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_18227527
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall�
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall>fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall:output:0"fused_avg_pool_conv2d_re_lu_182419*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_18233225
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall�
flatten/PartitionedCallPartitionedCall<fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_flatten_layer_call_and_return_conditional_losses_1823572
flatten/PartitionedCall�
#fused_dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0fused_dense_182423*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_fused_dense_layer_call_and_return_conditional_losses_1823872%
#fused_dense/StatefulPartitionedCall�
IdentityIdentity,fused_dense/StatefulPartitionedCall:output:04^fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall4^fused_max_pool_conv2d_re_lu/StatefulPartitionedCall6^fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::2j
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall2X
*fused_conv2d_re_lu/StatefulPartitionedCall*fused_conv2d_re_lu/StatefulPartitionedCall2J
#fused_dense/StatefulPartitionedCall#fused_dense/StatefulPartitionedCall2j
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall2n
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall:X T
/
_output_shapes
:���������  
!
_user_specified_name	input_1
�
�
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_182701
x)
%conv2d_conv2d_readvariableop_resource
identity��conv2d/Conv2D/ReadVariableOp�
zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d/Pad/paddings�
zero_padding2d/PadPadx$zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:���������""2
zero_padding2d/Pad�
conv2d/Conv2D/ReadVariableOpReadVariableOp%conv2d_conv2d_readvariableop_resource*&
_output_shapes
:<*
dtype02
conv2d/Conv2D/ReadVariableOp�
conv2d/Conv2DConv2Dzero_padding2d/Pad:output:0$conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������  <*
paddingVALID*
strides
2
conv2d/Conv2Dt
conv2d/ReluReluconv2d/Conv2D:output:0*
T0*/
_output_shapes
:���������  <2
conv2d/Relu�
clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
clamp/clip_by_value/Minimum/y�
clamp/clip_by_value/MinimumMinimumconv2d/Relu:activations:0&clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������  <2
clamp/clip_by_value/Minimums
clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp/clip_by_value/y�
clamp/clip_by_valueMaximumclamp/clip_by_value/Minimum:z:0clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������  <2
clamp/clip_by_value�
IdentityIdentityclamp/clip_by_value:z:0^conv2d/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������  <2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  :2<
conv2d/Conv2D/ReadVariableOpconv2d/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������  

_user_specified_namex
�

>__inference_fused_max_pool_conv2d_re_lu_1_layer_call_fn_182803
x
unknown
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������8*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *b
f]R[
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_1822752
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:���������82

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������<:22
StatefulPartitionedCallStatefulPartitionedCall:R N
/
_output_shapes
:���������<

_user_specified_namex
�
�
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182822
x+
'conv2d_3_conv2d_readvariableop_resource
identity��conv2d_3/Conv2D/ReadVariableOp�
average_pooling2d/AvgPoolAvgPoolx*
T0*/
_output_shapes
:���������8*
ksize
*
paddingVALID*
strides
2
average_pooling2d/AvgPool�
clamp_3/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_3/clip_by_value/Minimum/y�
clamp_3/clip_by_value/MinimumMinimum"average_pooling2d/AvgPool:output:0(clamp_3/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82
clamp_3/clip_by_value/Minimumw
clamp_3/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_3/clip_by_value/y�
clamp_3/clip_by_valueMaximum!clamp_3/clip_by_value/Minimum:z:0 clamp_3/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������82
clamp_3/clip_by_value�
zero_padding2d_3/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_3/Pad/paddings�
zero_padding2d_3/PadPadclamp_3/clip_by_value:z:0&zero_padding2d_3/Pad/paddings:output:0*
T0*/
_output_shapes
:���������82
zero_padding2d_3/Pad�
conv2d_3/Conv2D/ReadVariableOpReadVariableOp'conv2d_3_conv2d_readvariableop_resource*&
_output_shapes
:8*
dtype02 
conv2d_3/Conv2D/ReadVariableOp�
conv2d_3/Conv2DConv2Dzero_padding2d_3/Pad:output:0&conv2d_3/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2
conv2d_3/Conv2Dz
conv2d_3/ReluReluconv2d_3/Conv2D:output:0*
T0*/
_output_shapes
:���������2
conv2d_3/Relu�
clamp_4/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_4/clip_by_value/Minimum/y�
clamp_4/clip_by_value/MinimumMinimumconv2d_3/Relu:activations:0(clamp_4/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������2
clamp_4/clip_by_value/Minimumw
clamp_4/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_4/clip_by_value/y�
clamp_4/clip_by_valueMaximum!clamp_4/clip_by_value/Minimum:z:0 clamp_4/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������2
clamp_4/clip_by_value�
IdentityIdentityclamp_4/clip_by_value:z:0^conv2d_3/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������8:2@
conv2d_3/Conv2D/ReadVariableOpconv2d_3/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������8

_user_specified_namex
�
�
G__inference_fused_dense_layer_call_and_return_conditional_losses_182888
x(
$dense_matmul_readvariableop_resource
identity��dense/MatMul/ReadVariableOp�
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02
dense/MatMul/ReadVariableOp�
dense/MatMulMatMulx#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense/MatMul�
clamp_5/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2!
clamp_5/clip_by_value/Minimum/y�
clamp_5/clip_by_value/MinimumMinimumdense/MatMul:product:0(clamp_5/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:���������
2
clamp_5/clip_by_value/Minimumw
clamp_5/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   �2
clamp_5/clip_by_value/y�
clamp_5/clip_by_valueMaximum!clamp_5/clip_by_value/Minimum:z:0 clamp_5/clip_by_value/y:output:0*
T0*'
_output_shapes
:���������
2
clamp_5/clip_by_value�
IdentityIdentityclamp_5/clip_by_value:z:0^dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*+
_input_shapes
:����������:2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp:K G
(
_output_shapes
:����������

_user_specified_namex
�
D
(__inference_flatten_layer_call_fn_182866

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_flatten_layer_call_and_return_conditional_losses_1823572
PartitionedCallm
IdentityIdentityPartitionedCall:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������:W S
/
_output_shapes
:���������
 
_user_specified_nameinputs
�=
�
__inference__traced_save_182997
file_prefix%
!savev2_beta_1_read_readvariableop%
!savev2_beta_2_read_readvariableop$
 savev2_decay_read_readvariableop,
(savev2_learning_rate_read_readvariableop(
$savev2_adam_iter_read_readvariableop	?
;savev2_fused_conv2d_re_lu_conv2d_kernel_read_readvariableopJ
Fsavev2_fused_max_pool_conv2d_re_lu_conv2d_1_kernel_read_readvariableopL
Hsavev2_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernel_read_readvariableopJ
Fsavev2_fused_avg_pool_conv2d_re_lu_conv2d_3_kernel_read_readvariableop7
3savev2_fused_dense_dense_kernel_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableopF
Bsavev2_adam_fused_conv2d_re_lu_conv2d_kernel_m_read_readvariableopQ
Msavev2_adam_fused_max_pool_conv2d_re_lu_conv2d_1_kernel_m_read_readvariableopS
Osavev2_adam_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernel_m_read_readvariableopQ
Msavev2_adam_fused_avg_pool_conv2d_re_lu_conv2d_3_kernel_m_read_readvariableop>
:savev2_adam_fused_dense_dense_kernel_m_read_readvariableopF
Bsavev2_adam_fused_conv2d_re_lu_conv2d_kernel_v_read_readvariableopQ
Msavev2_adam_fused_max_pool_conv2d_re_lu_conv2d_1_kernel_v_read_readvariableopS
Osavev2_adam_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernel_v_read_readvariableopQ
Msavev2_adam_fused_avg_pool_conv2d_re_lu_conv2d_3_kernel_v_read_readvariableop>
:savev2_adam_fused_dense_dense_kernel_v_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�

value�
B�
B+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB&variables/0/.ATTRIBUTES/VARIABLE_VALUEB&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEB&variables/4/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/4/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/4/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0!savev2_beta_1_read_readvariableop!savev2_beta_2_read_readvariableop savev2_decay_read_readvariableop(savev2_learning_rate_read_readvariableop$savev2_adam_iter_read_readvariableop;savev2_fused_conv2d_re_lu_conv2d_kernel_read_readvariableopFsavev2_fused_max_pool_conv2d_re_lu_conv2d_1_kernel_read_readvariableopHsavev2_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernel_read_readvariableopFsavev2_fused_avg_pool_conv2d_re_lu_conv2d_3_kernel_read_readvariableop3savev2_fused_dense_dense_kernel_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableopBsavev2_adam_fused_conv2d_re_lu_conv2d_kernel_m_read_readvariableopMsavev2_adam_fused_max_pool_conv2d_re_lu_conv2d_1_kernel_m_read_readvariableopOsavev2_adam_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernel_m_read_readvariableopMsavev2_adam_fused_avg_pool_conv2d_re_lu_conv2d_3_kernel_m_read_readvariableop:savev2_adam_fused_dense_dense_kernel_m_read_readvariableopBsavev2_adam_fused_conv2d_re_lu_conv2d_kernel_v_read_readvariableopMsavev2_adam_fused_max_pool_conv2d_re_lu_conv2d_1_kernel_v_read_readvariableopOsavev2_adam_fused_max_pool_conv2d_re_lu_1_conv2d_2_kernel_v_read_readvariableopMsavev2_adam_fused_avg_pool_conv2d_re_lu_conv2d_3_kernel_v_read_readvariableop:savev2_adam_fused_dense_dense_kernel_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *'
dtypes
2	2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*�
_input_shapes�
�: : : : : : :<:<<:<8:8:	�
: : : : :<:<<:<8:8:	�
:<:<<:<8:8:	�
: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :,(
&
_output_shapes
:<:,(
&
_output_shapes
:<<:,(
&
_output_shapes
:<8:,	(
&
_output_shapes
:8:%
!

_output_shapes
:	�
:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :,(
&
_output_shapes
:<:,(
&
_output_shapes
:<<:,(
&
_output_shapes
:<8:,(
&
_output_shapes
:8:%!

_output_shapes
:	�
:,(
&
_output_shapes
:<:,(
&
_output_shapes
:<<:,(
&
_output_shapes
:<8:,(
&
_output_shapes
:8:%!

_output_shapes
:	�
:

_output_shapes
: 
�
J
.__inference_max_pooling2d_layer_call_fn_182082

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *R
fMRK
I__inference_max_pooling2d_layer_call_and_return_conditional_losses_1820762
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_182789
x+
'conv2d_2_conv2d_readvariableop_resource
identity��conv2d_2/Conv2D/ReadVariableOp�
max_pooling2d_1/MaxPoolMaxPoolx*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
2
max_pooling2d_1/MaxPool�
zero_padding2d_2/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_2/Pad/paddings�
zero_padding2d_2/PadPad max_pooling2d_1/MaxPool:output:0&zero_padding2d_2/Pad/paddings:output:0*
T0*/
_output_shapes
:���������

<2
zero_padding2d_2/Pad�
conv2d_2/Conv2D/ReadVariableOpReadVariableOp'conv2d_2_conv2d_readvariableop_resource*&
_output_shapes
:<8*
dtype02 
conv2d_2/Conv2D/ReadVariableOp�
conv2d_2/Conv2DConv2Dzero_padding2d_2/Pad:output:0&conv2d_2/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������8*
paddingVALID*
strides
2
conv2d_2/Conv2Dz
conv2d_2/ReluReluconv2d_2/Conv2D:output:0*
T0*/
_output_shapes
:���������82
conv2d_2/Relu�
clamp_2/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_2/clip_by_value/Minimum/y�
clamp_2/clip_by_value/MinimumMinimumconv2d_2/Relu:activations:0(clamp_2/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82
clamp_2/clip_by_value/Minimumw
clamp_2/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_2/clip_by_value/y�
clamp_2/clip_by_valueMaximum!clamp_2/clip_by_value/Minimum:z:0 clamp_2/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������82
clamp_2/clip_by_value�
IdentityIdentityclamp_2/clip_by_value:z:0^conv2d_2/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������82

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������<:2@
conv2d_2/Conv2D/ReadVariableOpconv2d_2/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������<

_user_specified_namex
�
�
G__inference_fused_dense_layer_call_and_return_conditional_losses_182376
x(
$dense_matmul_readvariableop_resource
identity��dense/MatMul/ReadVariableOp�
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02
dense/MatMul/ReadVariableOp�
dense/MatMulMatMulx#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
dense/MatMul�
clamp_5/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2!
clamp_5/clip_by_value/Minimum/y�
clamp_5/clip_by_value/MinimumMinimumdense/MatMul:product:0(clamp_5/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:���������
2
clamp_5/clip_by_value/Minimumw
clamp_5/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   �2
clamp_5/clip_by_value/y�
clamp_5/clip_by_valueMaximum!clamp_5/clip_by_value/Minimum:z:0 clamp_5/clip_by_value/y:output:0*
T0*'
_output_shapes
:���������
2
clamp_5/clip_by_value�
IdentityIdentityclamp_5/clip_by_value:z:0^dense/MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*+
_input_shapes
:����������:2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp:K G
(
_output_shapes
:����������

_user_specified_namex
�~
�
!__inference__wrapped_model_182057
input_1G
Csequential_fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resourceR
Nsequential_fused_max_pool_conv2d_re_lu_conv2d_1_conv2d_readvariableop_resourceT
Psequential_fused_max_pool_conv2d_re_lu_1_conv2d_2_conv2d_readvariableop_resourceR
Nsequential_fused_avg_pool_conv2d_re_lu_conv2d_3_conv2d_readvariableop_resource?
;sequential_fused_dense_dense_matmul_readvariableop_resource
identity��Esequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp�:sequential/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp�2sequential/fused_dense/dense/MatMul/ReadVariableOp�Esequential/fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp�Gsequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp�
9sequential/fused_conv2d_re_lu/zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2;
9sequential/fused_conv2d_re_lu/zero_padding2d/Pad/paddings�
0sequential/fused_conv2d_re_lu/zero_padding2d/PadPadinput_1Bsequential/fused_conv2d_re_lu/zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:���������""22
0sequential/fused_conv2d_re_lu/zero_padding2d/Pad�
:sequential/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOpReadVariableOpCsequential_fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resource*&
_output_shapes
:<*
dtype02<
:sequential/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp�
+sequential/fused_conv2d_re_lu/conv2d/Conv2DConv2D9sequential/fused_conv2d_re_lu/zero_padding2d/Pad:output:0Bsequential/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������  <*
paddingVALID*
strides
2-
+sequential/fused_conv2d_re_lu/conv2d/Conv2D�
)sequential/fused_conv2d_re_lu/conv2d/ReluRelu4sequential/fused_conv2d_re_lu/conv2d/Conv2D:output:0*
T0*/
_output_shapes
:���������  <2+
)sequential/fused_conv2d_re_lu/conv2d/Relu�
;sequential/fused_conv2d_re_lu/clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2=
;sequential/fused_conv2d_re_lu/clamp/clip_by_value/Minimum/y�
9sequential/fused_conv2d_re_lu/clamp/clip_by_value/MinimumMinimum7sequential/fused_conv2d_re_lu/conv2d/Relu:activations:0Dsequential/fused_conv2d_re_lu/clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������  <2;
9sequential/fused_conv2d_re_lu/clamp/clip_by_value/Minimum�
3sequential/fused_conv2d_re_lu/clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��25
3sequential/fused_conv2d_re_lu/clamp/clip_by_value/y�
1sequential/fused_conv2d_re_lu/clamp/clip_by_valueMaximum=sequential/fused_conv2d_re_lu/clamp/clip_by_value/Minimum:z:0<sequential/fused_conv2d_re_lu/clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������  <23
1sequential/fused_conv2d_re_lu/clamp/clip_by_value�
<sequential/fused_max_pool_conv2d_re_lu/max_pooling2d/MaxPoolMaxPool5sequential/fused_conv2d_re_lu/clamp/clip_by_value:z:0*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
2>
<sequential/fused_max_pool_conv2d_re_lu/max_pooling2d/MaxPool�
Dsequential/fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2F
Dsequential/fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad/paddings�
;sequential/fused_max_pool_conv2d_re_lu/zero_padding2d_1/PadPadEsequential/fused_max_pool_conv2d_re_lu/max_pooling2d/MaxPool:output:0Msequential/fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad/paddings:output:0*
T0*/
_output_shapes
:���������<2=
;sequential/fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad�
Esequential/fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOpReadVariableOpNsequential_fused_max_pool_conv2d_re_lu_conv2d_1_conv2d_readvariableop_resource*&
_output_shapes
:<<*
dtype02G
Esequential/fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp�
6sequential/fused_max_pool_conv2d_re_lu/conv2d_1/Conv2DConv2DDsequential/fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad:output:0Msequential/fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<*
paddingVALID*
strides
28
6sequential/fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D�
4sequential/fused_max_pool_conv2d_re_lu/conv2d_1/ReluRelu?sequential/fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D:output:0*
T0*/
_output_shapes
:���������<26
4sequential/fused_max_pool_conv2d_re_lu/conv2d_1/Relu�
Fsequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2H
Fsequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum/y�
Dsequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/MinimumMinimumBsequential/fused_max_pool_conv2d_re_lu/conv2d_1/Relu:activations:0Osequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������<2F
Dsequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum�
>sequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2@
>sequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/y�
<sequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_valueMaximumHsequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum:z:0Gsequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������<2>
<sequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value�
@sequential/fused_max_pool_conv2d_re_lu_1/max_pooling2d_1/MaxPoolMaxPool@sequential/fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value:z:0*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
2B
@sequential/fused_max_pool_conv2d_re_lu_1/max_pooling2d_1/MaxPool�
Fsequential/fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2H
Fsequential/fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad/paddings�
=sequential/fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/PadPadIsequential/fused_max_pool_conv2d_re_lu_1/max_pooling2d_1/MaxPool:output:0Osequential/fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad/paddings:output:0*
T0*/
_output_shapes
:���������

<2?
=sequential/fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad�
Gsequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOpReadVariableOpPsequential_fused_max_pool_conv2d_re_lu_1_conv2d_2_conv2d_readvariableop_resource*&
_output_shapes
:<8*
dtype02I
Gsequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp�
8sequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2DConv2DFsequential/fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad:output:0Osequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������8*
paddingVALID*
strides
2:
8sequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D�
6sequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/ReluReluAsequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D:output:0*
T0*/
_output_shapes
:���������828
6sequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Relu�
Hsequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2J
Hsequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum/y�
Fsequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/MinimumMinimumDsequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Relu:activations:0Qsequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82H
Fsequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum�
@sequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2B
@sequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/y�
>sequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_valueMaximumJsequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum:z:0Isequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������82@
>sequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value�
@sequential/fused_avg_pool_conv2d_re_lu/average_pooling2d/AvgPoolAvgPoolBsequential/fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value:z:0*
T0*/
_output_shapes
:���������8*
ksize
*
paddingVALID*
strides
2B
@sequential/fused_avg_pool_conv2d_re_lu/average_pooling2d/AvgPool�
Fsequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2H
Fsequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum/y�
Dsequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/MinimumMinimumIsequential/fused_avg_pool_conv2d_re_lu/average_pooling2d/AvgPool:output:0Osequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82F
Dsequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum�
>sequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2@
>sequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/y�
<sequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_valueMaximumHsequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum:z:0Gsequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������82>
<sequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value�
Dsequential/fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2F
Dsequential/fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad/paddings�
;sequential/fused_avg_pool_conv2d_re_lu/zero_padding2d_3/PadPad@sequential/fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value:z:0Msequential/fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad/paddings:output:0*
T0*/
_output_shapes
:���������82=
;sequential/fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad�
Esequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOpReadVariableOpNsequential_fused_avg_pool_conv2d_re_lu_conv2d_3_conv2d_readvariableop_resource*&
_output_shapes
:8*
dtype02G
Esequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp�
6sequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2DConv2DDsequential/fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad:output:0Msequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
28
6sequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D�
4sequential/fused_avg_pool_conv2d_re_lu/conv2d_3/ReluRelu?sequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D:output:0*
T0*/
_output_shapes
:���������26
4sequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Relu�
Fsequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2H
Fsequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum/y�
Dsequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/MinimumMinimumBsequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Relu:activations:0Osequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������2F
Dsequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum�
>sequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2@
>sequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/y�
<sequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_valueMaximumHsequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum:z:0Gsequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������2>
<sequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value�
sequential/flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"�����   2
sequential/flatten/Const�
sequential/flatten/ReshapeReshape@sequential/fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value:z:0!sequential/flatten/Const:output:0*
T0*(
_output_shapes
:����������2
sequential/flatten/Reshape�
2sequential/fused_dense/dense/MatMul/ReadVariableOpReadVariableOp;sequential_fused_dense_dense_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype024
2sequential/fused_dense/dense/MatMul/ReadVariableOp�
#sequential/fused_dense/dense/MatMulMatMul#sequential/flatten/Reshape:output:0:sequential/fused_dense/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2%
#sequential/fused_dense/dense/MatMul�
6sequential/fused_dense/clamp_5/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H28
6sequential/fused_dense/clamp_5/clip_by_value/Minimum/y�
4sequential/fused_dense/clamp_5/clip_by_value/MinimumMinimum-sequential/fused_dense/dense/MatMul:product:0?sequential/fused_dense/clamp_5/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:���������
26
4sequential/fused_dense/clamp_5/clip_by_value/Minimum�
.sequential/fused_dense/clamp_5/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   �20
.sequential/fused_dense/clamp_5/clip_by_value/y�
,sequential/fused_dense/clamp_5/clip_by_valueMaximum8sequential/fused_dense/clamp_5/clip_by_value/Minimum:z:07sequential/fused_dense/clamp_5/clip_by_value/y:output:0*
T0*'
_output_shapes
:���������
2.
,sequential/fused_dense/clamp_5/clip_by_value�
IdentityIdentity0sequential/fused_dense/clamp_5/clip_by_value:z:0F^sequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp;^sequential/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp3^sequential/fused_dense/dense/MatMul/ReadVariableOpF^sequential/fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOpH^sequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::2�
Esequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOpEsequential/fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp2x
:sequential/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp:sequential/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp2h
2sequential/fused_dense/dense/MatMul/ReadVariableOp2sequential/fused_dense/dense/MatMul/ReadVariableOp2�
Esequential/fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOpEsequential/fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp2�
Gsequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOpGsequential/fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp:X T
/
_output_shapes
:���������  
!
_user_specified_name	input_1
�
}
<__inference_fused_avg_pool_conv2d_re_lu_layer_call_fn_182855
x
unknown
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_1823322
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������8:22
StatefulPartitionedCallStatefulPartitionedCall:R N
/
_output_shapes
:���������8

_user_specified_namex
�
L
0__inference_max_pooling2d_1_layer_call_fn_182107

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *T
fORM
K__inference_max_pooling2d_1_layer_call_and_return_conditional_losses_1821012
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�n
�
F__inference_sequential_layer_call_and_return_conditional_losses_182583

inputs<
8fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resourceG
Cfused_max_pool_conv2d_re_lu_conv2d_1_conv2d_readvariableop_resourceI
Efused_max_pool_conv2d_re_lu_1_conv2d_2_conv2d_readvariableop_resourceG
Cfused_avg_pool_conv2d_re_lu_conv2d_3_conv2d_readvariableop_resource4
0fused_dense_dense_matmul_readvariableop_resource
identity��:fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp�/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp�'fused_dense/dense/MatMul/ReadVariableOp�:fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp�<fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp�
.fused_conv2d_re_lu/zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             20
.fused_conv2d_re_lu/zero_padding2d/Pad/paddings�
%fused_conv2d_re_lu/zero_padding2d/PadPadinputs7fused_conv2d_re_lu/zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:���������""2'
%fused_conv2d_re_lu/zero_padding2d/Pad�
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOpReadVariableOp8fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resource*&
_output_shapes
:<*
dtype021
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp�
 fused_conv2d_re_lu/conv2d/Conv2DConv2D.fused_conv2d_re_lu/zero_padding2d/Pad:output:07fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������  <*
paddingVALID*
strides
2"
 fused_conv2d_re_lu/conv2d/Conv2D�
fused_conv2d_re_lu/conv2d/ReluRelu)fused_conv2d_re_lu/conv2d/Conv2D:output:0*
T0*/
_output_shapes
:���������  <2 
fused_conv2d_re_lu/conv2d/Relu�
0fused_conv2d_re_lu/clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?22
0fused_conv2d_re_lu/clamp/clip_by_value/Minimum/y�
.fused_conv2d_re_lu/clamp/clip_by_value/MinimumMinimum,fused_conv2d_re_lu/conv2d/Relu:activations:09fused_conv2d_re_lu/clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������  <20
.fused_conv2d_re_lu/clamp/clip_by_value/Minimum�
(fused_conv2d_re_lu/clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2*
(fused_conv2d_re_lu/clamp/clip_by_value/y�
&fused_conv2d_re_lu/clamp/clip_by_valueMaximum2fused_conv2d_re_lu/clamp/clip_by_value/Minimum:z:01fused_conv2d_re_lu/clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������  <2(
&fused_conv2d_re_lu/clamp/clip_by_value�
1fused_max_pool_conv2d_re_lu/max_pooling2d/MaxPoolMaxPool*fused_conv2d_re_lu/clamp/clip_by_value:z:0*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
23
1fused_max_pool_conv2d_re_lu/max_pooling2d/MaxPool�
9fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2;
9fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad/paddings�
0fused_max_pool_conv2d_re_lu/zero_padding2d_1/PadPad:fused_max_pool_conv2d_re_lu/max_pooling2d/MaxPool:output:0Bfused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad/paddings:output:0*
T0*/
_output_shapes
:���������<22
0fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad�
:fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOpReadVariableOpCfused_max_pool_conv2d_re_lu_conv2d_1_conv2d_readvariableop_resource*&
_output_shapes
:<<*
dtype02<
:fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp�
+fused_max_pool_conv2d_re_lu/conv2d_1/Conv2DConv2D9fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad:output:0Bfused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<*
paddingVALID*
strides
2-
+fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D�
)fused_max_pool_conv2d_re_lu/conv2d_1/ReluRelu4fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D:output:0*
T0*/
_output_shapes
:���������<2+
)fused_max_pool_conv2d_re_lu/conv2d_1/Relu�
;fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2=
;fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum/y�
9fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/MinimumMinimum7fused_max_pool_conv2d_re_lu/conv2d_1/Relu:activations:0Dfused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������<2;
9fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum�
3fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��25
3fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/y�
1fused_max_pool_conv2d_re_lu/clamp_1/clip_by_valueMaximum=fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum:z:0<fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������<23
1fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value�
5fused_max_pool_conv2d_re_lu_1/max_pooling2d_1/MaxPoolMaxPool5fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value:z:0*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
27
5fused_max_pool_conv2d_re_lu_1/max_pooling2d_1/MaxPool�
;fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2=
;fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad/paddings�
2fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/PadPad>fused_max_pool_conv2d_re_lu_1/max_pooling2d_1/MaxPool:output:0Dfused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad/paddings:output:0*
T0*/
_output_shapes
:���������

<24
2fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad�
<fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOpReadVariableOpEfused_max_pool_conv2d_re_lu_1_conv2d_2_conv2d_readvariableop_resource*&
_output_shapes
:<8*
dtype02>
<fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp�
-fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2DConv2D;fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad:output:0Dfused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������8*
paddingVALID*
strides
2/
-fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D�
+fused_max_pool_conv2d_re_lu_1/conv2d_2/ReluRelu6fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D:output:0*
T0*/
_output_shapes
:���������82-
+fused_max_pool_conv2d_re_lu_1/conv2d_2/Relu�
=fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2?
=fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum/y�
;fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/MinimumMinimum9fused_max_pool_conv2d_re_lu_1/conv2d_2/Relu:activations:0Ffused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82=
;fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum�
5fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��27
5fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/y�
3fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_valueMaximum?fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum:z:0>fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������825
3fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value�
5fused_avg_pool_conv2d_re_lu/average_pooling2d/AvgPoolAvgPool7fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value:z:0*
T0*/
_output_shapes
:���������8*
ksize
*
paddingVALID*
strides
27
5fused_avg_pool_conv2d_re_lu/average_pooling2d/AvgPool�
;fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2=
;fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum/y�
9fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/MinimumMinimum>fused_avg_pool_conv2d_re_lu/average_pooling2d/AvgPool:output:0Dfused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82;
9fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum�
3fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��25
3fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/y�
1fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_valueMaximum=fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum:z:0<fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������823
1fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value�
9fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2;
9fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad/paddings�
0fused_avg_pool_conv2d_re_lu/zero_padding2d_3/PadPad5fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value:z:0Bfused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad/paddings:output:0*
T0*/
_output_shapes
:���������822
0fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad�
:fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOpReadVariableOpCfused_avg_pool_conv2d_re_lu_conv2d_3_conv2d_readvariableop_resource*&
_output_shapes
:8*
dtype02<
:fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp�
+fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2DConv2D9fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad:output:0Bfused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2-
+fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D�
)fused_avg_pool_conv2d_re_lu/conv2d_3/ReluRelu4fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D:output:0*
T0*/
_output_shapes
:���������2+
)fused_avg_pool_conv2d_re_lu/conv2d_3/Relu�
;fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2=
;fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum/y�
9fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/MinimumMinimum7fused_avg_pool_conv2d_re_lu/conv2d_3/Relu:activations:0Dfused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������2;
9fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum�
3fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��25
3fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/y�
1fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_valueMaximum=fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum:z:0<fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������23
1fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_valueo
flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"�����   2
flatten/Const�
flatten/ReshapeReshape5fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value:z:0flatten/Const:output:0*
T0*(
_output_shapes
:����������2
flatten/Reshape�
'fused_dense/dense/MatMul/ReadVariableOpReadVariableOp0fused_dense_dense_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02)
'fused_dense/dense/MatMul/ReadVariableOp�
fused_dense/dense/MatMulMatMulflatten/Reshape:output:0/fused_dense/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
fused_dense/dense/MatMul�
+fused_dense/clamp_5/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2-
+fused_dense/clamp_5/clip_by_value/Minimum/y�
)fused_dense/clamp_5/clip_by_value/MinimumMinimum"fused_dense/dense/MatMul:product:04fused_dense/clamp_5/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:���������
2+
)fused_dense/clamp_5/clip_by_value/Minimum�
#fused_dense/clamp_5/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   �2%
#fused_dense/clamp_5/clip_by_value/y�
!fused_dense/clamp_5/clip_by_valueMaximum-fused_dense/clamp_5/clip_by_value/Minimum:z:0,fused_dense/clamp_5/clip_by_value/y:output:0*
T0*'
_output_shapes
:���������
2#
!fused_dense/clamp_5/clip_by_value�
IdentityIdentity%fused_dense/clamp_5/clip_by_value:z:0;^fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp0^fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp(^fused_dense/dense/MatMul/ReadVariableOp;^fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp=^fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::2x
:fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp:fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp2b
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp2R
'fused_dense/dense/MatMul/ReadVariableOp'fused_dense/dense/MatMul/ReadVariableOp2x
:fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp:fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp2|
<fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp<fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp:W S
/
_output_shapes
:���������  
 
_user_specified_nameinputs
�
f
J__inference_zero_padding2d_layer_call_and_return_conditional_losses_182064

inputs
identity�
Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
Pad/paddings�
PadPadinputsPad/paddings:output:0*
T0*J
_output_shapes8
6:4������������������������������������2
Pad�
IdentityIdentityPad:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
K
/__inference_zero_padding2d_layer_call_fn_182070

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_zero_padding2d_layer_call_and_return_conditional_losses_1820642
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182211
x+
'conv2d_1_conv2d_readvariableop_resource
identity��conv2d_1/Conv2D/ReadVariableOp�
max_pooling2d/MaxPoolMaxPoolx*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
2
max_pooling2d/MaxPool�
zero_padding2d_1/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_1/Pad/paddings�
zero_padding2d_1/PadPadmax_pooling2d/MaxPool:output:0&zero_padding2d_1/Pad/paddings:output:0*
T0*/
_output_shapes
:���������<2
zero_padding2d_1/Pad�
conv2d_1/Conv2D/ReadVariableOpReadVariableOp'conv2d_1_conv2d_readvariableop_resource*&
_output_shapes
:<<*
dtype02 
conv2d_1/Conv2D/ReadVariableOp�
conv2d_1/Conv2DConv2Dzero_padding2d_1/Pad:output:0&conv2d_1/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<*
paddingVALID*
strides
2
conv2d_1/Conv2Dz
conv2d_1/ReluReluconv2d_1/Conv2D:output:0*
T0*/
_output_shapes
:���������<2
conv2d_1/Relu�
clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_1/clip_by_value/Minimum/y�
clamp_1/clip_by_value/MinimumMinimumconv2d_1/Relu:activations:0(clamp_1/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������<2
clamp_1/clip_by_value/Minimumw
clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_1/clip_by_value/y�
clamp_1/clip_by_valueMaximum!clamp_1/clip_by_value/Minimum:z:0 clamp_1/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������<2
clamp_1/clip_by_value�
IdentityIdentityclamp_1/clip_by_value:z:0^conv2d_1/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������<2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  <:2@
conv2d_1/Conv2D/ReadVariableOpconv2d_1/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������  <

_user_specified_namex
�n
�
F__inference_sequential_layer_call_and_return_conditional_losses_182643

inputs<
8fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resourceG
Cfused_max_pool_conv2d_re_lu_conv2d_1_conv2d_readvariableop_resourceI
Efused_max_pool_conv2d_re_lu_1_conv2d_2_conv2d_readvariableop_resourceG
Cfused_avg_pool_conv2d_re_lu_conv2d_3_conv2d_readvariableop_resource4
0fused_dense_dense_matmul_readvariableop_resource
identity��:fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp�/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp�'fused_dense/dense/MatMul/ReadVariableOp�:fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp�<fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp�
.fused_conv2d_re_lu/zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             20
.fused_conv2d_re_lu/zero_padding2d/Pad/paddings�
%fused_conv2d_re_lu/zero_padding2d/PadPadinputs7fused_conv2d_re_lu/zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:���������""2'
%fused_conv2d_re_lu/zero_padding2d/Pad�
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOpReadVariableOp8fused_conv2d_re_lu_conv2d_conv2d_readvariableop_resource*&
_output_shapes
:<*
dtype021
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp�
 fused_conv2d_re_lu/conv2d/Conv2DConv2D.fused_conv2d_re_lu/zero_padding2d/Pad:output:07fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������  <*
paddingVALID*
strides
2"
 fused_conv2d_re_lu/conv2d/Conv2D�
fused_conv2d_re_lu/conv2d/ReluRelu)fused_conv2d_re_lu/conv2d/Conv2D:output:0*
T0*/
_output_shapes
:���������  <2 
fused_conv2d_re_lu/conv2d/Relu�
0fused_conv2d_re_lu/clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?22
0fused_conv2d_re_lu/clamp/clip_by_value/Minimum/y�
.fused_conv2d_re_lu/clamp/clip_by_value/MinimumMinimum,fused_conv2d_re_lu/conv2d/Relu:activations:09fused_conv2d_re_lu/clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������  <20
.fused_conv2d_re_lu/clamp/clip_by_value/Minimum�
(fused_conv2d_re_lu/clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2*
(fused_conv2d_re_lu/clamp/clip_by_value/y�
&fused_conv2d_re_lu/clamp/clip_by_valueMaximum2fused_conv2d_re_lu/clamp/clip_by_value/Minimum:z:01fused_conv2d_re_lu/clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������  <2(
&fused_conv2d_re_lu/clamp/clip_by_value�
1fused_max_pool_conv2d_re_lu/max_pooling2d/MaxPoolMaxPool*fused_conv2d_re_lu/clamp/clip_by_value:z:0*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
23
1fused_max_pool_conv2d_re_lu/max_pooling2d/MaxPool�
9fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2;
9fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad/paddings�
0fused_max_pool_conv2d_re_lu/zero_padding2d_1/PadPad:fused_max_pool_conv2d_re_lu/max_pooling2d/MaxPool:output:0Bfused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad/paddings:output:0*
T0*/
_output_shapes
:���������<22
0fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad�
:fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOpReadVariableOpCfused_max_pool_conv2d_re_lu_conv2d_1_conv2d_readvariableop_resource*&
_output_shapes
:<<*
dtype02<
:fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp�
+fused_max_pool_conv2d_re_lu/conv2d_1/Conv2DConv2D9fused_max_pool_conv2d_re_lu/zero_padding2d_1/Pad:output:0Bfused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<*
paddingVALID*
strides
2-
+fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D�
)fused_max_pool_conv2d_re_lu/conv2d_1/ReluRelu4fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D:output:0*
T0*/
_output_shapes
:���������<2+
)fused_max_pool_conv2d_re_lu/conv2d_1/Relu�
;fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2=
;fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum/y�
9fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/MinimumMinimum7fused_max_pool_conv2d_re_lu/conv2d_1/Relu:activations:0Dfused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������<2;
9fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum�
3fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��25
3fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/y�
1fused_max_pool_conv2d_re_lu/clamp_1/clip_by_valueMaximum=fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/Minimum:z:0<fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������<23
1fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value�
5fused_max_pool_conv2d_re_lu_1/max_pooling2d_1/MaxPoolMaxPool5fused_max_pool_conv2d_re_lu/clamp_1/clip_by_value:z:0*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
27
5fused_max_pool_conv2d_re_lu_1/max_pooling2d_1/MaxPool�
;fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2=
;fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad/paddings�
2fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/PadPad>fused_max_pool_conv2d_re_lu_1/max_pooling2d_1/MaxPool:output:0Dfused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad/paddings:output:0*
T0*/
_output_shapes
:���������

<24
2fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad�
<fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOpReadVariableOpEfused_max_pool_conv2d_re_lu_1_conv2d_2_conv2d_readvariableop_resource*&
_output_shapes
:<8*
dtype02>
<fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp�
-fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2DConv2D;fused_max_pool_conv2d_re_lu_1/zero_padding2d_2/Pad:output:0Dfused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������8*
paddingVALID*
strides
2/
-fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D�
+fused_max_pool_conv2d_re_lu_1/conv2d_2/ReluRelu6fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D:output:0*
T0*/
_output_shapes
:���������82-
+fused_max_pool_conv2d_re_lu_1/conv2d_2/Relu�
=fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2?
=fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum/y�
;fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/MinimumMinimum9fused_max_pool_conv2d_re_lu_1/conv2d_2/Relu:activations:0Ffused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82=
;fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum�
5fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��27
5fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/y�
3fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_valueMaximum?fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/Minimum:z:0>fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������825
3fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value�
5fused_avg_pool_conv2d_re_lu/average_pooling2d/AvgPoolAvgPool7fused_max_pool_conv2d_re_lu_1/clamp_2/clip_by_value:z:0*
T0*/
_output_shapes
:���������8*
ksize
*
paddingVALID*
strides
27
5fused_avg_pool_conv2d_re_lu/average_pooling2d/AvgPool�
;fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2=
;fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum/y�
9fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/MinimumMinimum>fused_avg_pool_conv2d_re_lu/average_pooling2d/AvgPool:output:0Dfused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82;
9fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum�
3fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��25
3fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/y�
1fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_valueMaximum=fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/Minimum:z:0<fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������823
1fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value�
9fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2;
9fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad/paddings�
0fused_avg_pool_conv2d_re_lu/zero_padding2d_3/PadPad5fused_avg_pool_conv2d_re_lu/clamp_3/clip_by_value:z:0Bfused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad/paddings:output:0*
T0*/
_output_shapes
:���������822
0fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad�
:fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOpReadVariableOpCfused_avg_pool_conv2d_re_lu_conv2d_3_conv2d_readvariableop_resource*&
_output_shapes
:8*
dtype02<
:fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp�
+fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2DConv2D9fused_avg_pool_conv2d_re_lu/zero_padding2d_3/Pad:output:0Bfused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2-
+fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D�
)fused_avg_pool_conv2d_re_lu/conv2d_3/ReluRelu4fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D:output:0*
T0*/
_output_shapes
:���������2+
)fused_avg_pool_conv2d_re_lu/conv2d_3/Relu�
;fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2=
;fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum/y�
9fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/MinimumMinimum7fused_avg_pool_conv2d_re_lu/conv2d_3/Relu:activations:0Dfused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������2;
9fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum�
3fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��25
3fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/y�
1fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_valueMaximum=fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/Minimum:z:0<fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������23
1fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_valueo
flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"�����   2
flatten/Const�
flatten/ReshapeReshape5fused_avg_pool_conv2d_re_lu/clamp_4/clip_by_value:z:0flatten/Const:output:0*
T0*(
_output_shapes
:����������2
flatten/Reshape�
'fused_dense/dense/MatMul/ReadVariableOpReadVariableOp0fused_dense_dense_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype02)
'fused_dense/dense/MatMul/ReadVariableOp�
fused_dense/dense/MatMulMatMulflatten/Reshape:output:0/fused_dense/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
2
fused_dense/dense/MatMul�
+fused_dense/clamp_5/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *   H2-
+fused_dense/clamp_5/clip_by_value/Minimum/y�
)fused_dense/clamp_5/clip_by_value/MinimumMinimum"fused_dense/dense/MatMul:product:04fused_dense/clamp_5/clip_by_value/Minimum/y:output:0*
T0*'
_output_shapes
:���������
2+
)fused_dense/clamp_5/clip_by_value/Minimum�
#fused_dense/clamp_5/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *   �2%
#fused_dense/clamp_5/clip_by_value/y�
!fused_dense/clamp_5/clip_by_valueMaximum-fused_dense/clamp_5/clip_by_value/Minimum:z:0,fused_dense/clamp_5/clip_by_value/y:output:0*
T0*'
_output_shapes
:���������
2#
!fused_dense/clamp_5/clip_by_value�
IdentityIdentity%fused_dense/clamp_5/clip_by_value:z:0;^fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp0^fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp(^fused_dense/dense/MatMul/ReadVariableOp;^fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp=^fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::2x
:fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp:fused_avg_pool_conv2d_re_lu/conv2d_3/Conv2D/ReadVariableOp2b
/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp/fused_conv2d_re_lu/conv2d/Conv2D/ReadVariableOp2R
'fused_dense/dense/MatMul/ReadVariableOp'fused_dense/dense/MatMul/ReadVariableOp2x
:fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp:fused_max_pool_conv2d_re_lu/conv2d_1/Conv2D/ReadVariableOp2|
<fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp<fused_max_pool_conv2d_re_lu_1/conv2d_2/Conv2D/ReadVariableOp:W S
/
_output_shapes
:���������  
 
_user_specified_nameinputs
�
�
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182313
x+
'conv2d_3_conv2d_readvariableop_resource
identity��conv2d_3/Conv2D/ReadVariableOp�
average_pooling2d/AvgPoolAvgPoolx*
T0*/
_output_shapes
:���������8*
ksize
*
paddingVALID*
strides
2
average_pooling2d/AvgPool�
clamp_3/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_3/clip_by_value/Minimum/y�
clamp_3/clip_by_value/MinimumMinimum"average_pooling2d/AvgPool:output:0(clamp_3/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82
clamp_3/clip_by_value/Minimumw
clamp_3/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_3/clip_by_value/y�
clamp_3/clip_by_valueMaximum!clamp_3/clip_by_value/Minimum:z:0 clamp_3/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������82
clamp_3/clip_by_value�
zero_padding2d_3/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_3/Pad/paddings�
zero_padding2d_3/PadPadclamp_3/clip_by_value:z:0&zero_padding2d_3/Pad/paddings:output:0*
T0*/
_output_shapes
:���������82
zero_padding2d_3/Pad�
conv2d_3/Conv2D/ReadVariableOpReadVariableOp'conv2d_3_conv2d_readvariableop_resource*&
_output_shapes
:8*
dtype02 
conv2d_3/Conv2D/ReadVariableOp�
conv2d_3/Conv2DConv2Dzero_padding2d_3/Pad:output:0&conv2d_3/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2
conv2d_3/Conv2Dz
conv2d_3/ReluReluconv2d_3/Conv2D:output:0*
T0*/
_output_shapes
:���������2
conv2d_3/Relu�
clamp_4/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_4/clip_by_value/Minimum/y�
clamp_4/clip_by_value/MinimumMinimumconv2d_3/Relu:activations:0(clamp_4/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������2
clamp_4/clip_by_value/Minimumw
clamp_4/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_4/clip_by_value/y�
clamp_4/clip_by_valueMaximum!clamp_4/clip_by_value/Minimum:z:0 clamp_4/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������2
clamp_4/clip_by_value�
IdentityIdentityclamp_4/clip_by_value:z:0^conv2d_3/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������8:2@
conv2d_3/Conv2D/ReadVariableOpconv2d_3/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������8

_user_specified_namex
�
h
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_182139

inputs
identity�
Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
Pad/paddings�
PadPadinputsPad/paddings:output:0*
T0*J
_output_shapes8
6:4������������������������������������2
Pad�
IdentityIdentityPad:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
M
1__inference_zero_padding2d_1_layer_call_fn_182095

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *U
fPRN
L__inference_zero_padding2d_1_layer_call_and_return_conditional_losses_1820892
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
M
1__inference_zero_padding2d_2_layer_call_fn_182120

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *J
_output_shapes8
6:4������������������������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *U
fPRN
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_1821142
PartitionedCall�
IdentityIdentityPartitionedCall:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
m
,__inference_fused_dense_layer_call_fn_182895
x
unknown
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_fused_dense_layer_call_and_return_conditional_losses_1823872
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*+
_input_shapes
:����������:22
StatefulPartitionedCallStatefulPartitionedCall:K G
(
_output_shapes
:����������

_user_specified_namex
�"
�
F__inference_sequential_layer_call_and_return_conditional_losses_182450

inputs
fused_conv2d_re_lu_182433&
"fused_max_pool_conv2d_re_lu_182436(
$fused_max_pool_conv2d_re_lu_1_182439&
"fused_avg_pool_conv2d_re_lu_182442
fused_dense_182446
identity��3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall�*fused_conv2d_re_lu/StatefulPartitionedCall�#fused_dense/StatefulPartitionedCall�3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall�5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall�
*fused_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCallinputsfused_conv2d_re_lu_182433*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������  <*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_1821632,
*fused_conv2d_re_lu/StatefulPartitionedCall�
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall3fused_conv2d_re_lu/StatefulPartitionedCall:output:0"fused_max_pool_conv2d_re_lu_182436*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������<*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_18221125
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall�
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCallStatefulPartitionedCall<fused_max_pool_conv2d_re_lu/StatefulPartitionedCall:output:0$fused_max_pool_conv2d_re_lu_1_182439*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������8*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *b
f]R[
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_18226027
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall�
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCallStatefulPartitionedCall>fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall:output:0"fused_avg_pool_conv2d_re_lu_182442*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *`
f[RY
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_18231325
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall�
flatten/PartitionedCallPartitionedCall<fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_flatten_layer_call_and_return_conditional_losses_1823572
flatten/PartitionedCall�
#fused_dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0fused_dense_182446*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_fused_dense_layer_call_and_return_conditional_losses_1823762%
#fused_dense/StatefulPartitionedCall�
IdentityIdentity,fused_dense/StatefulPartitionedCall:output:04^fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall+^fused_conv2d_re_lu/StatefulPartitionedCall$^fused_dense/StatefulPartitionedCall4^fused_max_pool_conv2d_re_lu/StatefulPartitionedCall6^fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*B
_input_shapes1
/:���������  :::::2j
3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall3fused_avg_pool_conv2d_re_lu/StatefulPartitionedCall2X
*fused_conv2d_re_lu/StatefulPartitionedCall*fused_conv2d_re_lu/StatefulPartitionedCall2J
#fused_dense/StatefulPartitionedCall#fused_dense/StatefulPartitionedCall2j
3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall3fused_max_pool_conv2d_re_lu/StatefulPartitionedCall2n
5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall5fused_max_pool_conv2d_re_lu_1/StatefulPartitionedCall:W S
/
_output_shapes
:���������  
 
_user_specified_nameinputs
�
�
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182841
x+
'conv2d_3_conv2d_readvariableop_resource
identity��conv2d_3/Conv2D/ReadVariableOp�
average_pooling2d/AvgPoolAvgPoolx*
T0*/
_output_shapes
:���������8*
ksize
*
paddingVALID*
strides
2
average_pooling2d/AvgPool�
clamp_3/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_3/clip_by_value/Minimum/y�
clamp_3/clip_by_value/MinimumMinimum"average_pooling2d/AvgPool:output:0(clamp_3/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82
clamp_3/clip_by_value/Minimumw
clamp_3/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_3/clip_by_value/y�
clamp_3/clip_by_valueMaximum!clamp_3/clip_by_value/Minimum:z:0 clamp_3/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������82
clamp_3/clip_by_value�
zero_padding2d_3/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_3/Pad/paddings�
zero_padding2d_3/PadPadclamp_3/clip_by_value:z:0&zero_padding2d_3/Pad/paddings:output:0*
T0*/
_output_shapes
:���������82
zero_padding2d_3/Pad�
conv2d_3/Conv2D/ReadVariableOpReadVariableOp'conv2d_3_conv2d_readvariableop_resource*&
_output_shapes
:8*
dtype02 
conv2d_3/Conv2D/ReadVariableOp�
conv2d_3/Conv2DConv2Dzero_padding2d_3/Pad:output:0&conv2d_3/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������*
paddingVALID*
strides
2
conv2d_3/Conv2Dz
conv2d_3/ReluReluconv2d_3/Conv2D:output:0*
T0*/
_output_shapes
:���������2
conv2d_3/Relu�
clamp_4/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_4/clip_by_value/Minimum/y�
clamp_4/clip_by_value/MinimumMinimumconv2d_3/Relu:activations:0(clamp_4/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������2
clamp_4/clip_by_value/Minimumw
clamp_4/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_4/clip_by_value/y�
clamp_4/clip_by_valueMaximum!clamp_4/clip_by_value/Minimum:z:0 clamp_4/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������2
clamp_4/clip_by_value�
IdentityIdentityclamp_4/clip_by_value:z:0^conv2d_3/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������8:2@
conv2d_3/Conv2D/ReadVariableOpconv2d_3/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������8

_user_specified_namex
�
g
K__inference_max_pooling2d_1_layer_call_and_return_conditional_losses_182101

inputs
identity�
MaxPoolMaxPoolinputs*J
_output_shapes8
6:4������������������������������������*
ksize
*
paddingVALID*
strides
2	
MaxPool�
IdentityIdentityMaxPool:output:0*
T0*J
_output_shapes8
6:4������������������������������������2

Identity"
identityIdentity:output:0*I
_input_shapes8
6:4������������������������������������:r n
J
_output_shapes8
6:4������������������������������������
 
_user_specified_nameinputs
�
�
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_182774
x+
'conv2d_2_conv2d_readvariableop_resource
identity��conv2d_2/Conv2D/ReadVariableOp�
max_pooling2d_1/MaxPoolMaxPoolx*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
2
max_pooling2d_1/MaxPool�
zero_padding2d_2/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_2/Pad/paddings�
zero_padding2d_2/PadPad max_pooling2d_1/MaxPool:output:0&zero_padding2d_2/Pad/paddings:output:0*
T0*/
_output_shapes
:���������

<2
zero_padding2d_2/Pad�
conv2d_2/Conv2D/ReadVariableOpReadVariableOp'conv2d_2_conv2d_readvariableop_resource*&
_output_shapes
:<8*
dtype02 
conv2d_2/Conv2D/ReadVariableOp�
conv2d_2/Conv2DConv2Dzero_padding2d_2/Pad:output:0&conv2d_2/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������8*
paddingVALID*
strides
2
conv2d_2/Conv2Dz
conv2d_2/ReluReluconv2d_2/Conv2D:output:0*
T0*/
_output_shapes
:���������82
conv2d_2/Relu�
clamp_2/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_2/clip_by_value/Minimum/y�
clamp_2/clip_by_value/MinimumMinimumconv2d_2/Relu:activations:0(clamp_2/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������82
clamp_2/clip_by_value/Minimumw
clamp_2/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_2/clip_by_value/y�
clamp_2/clip_by_valueMaximum!clamp_2/clip_by_value/Minimum:z:0 clamp_2/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������82
clamp_2/clip_by_value�
IdentityIdentityclamp_2/clip_by_value:z:0^conv2d_2/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������82

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������<:2@
conv2d_2/Conv2D/ReadVariableOpconv2d_2/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������<

_user_specified_namex
�
�
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_182177
x)
%conv2d_conv2d_readvariableop_resource
identity��conv2d/Conv2D/ReadVariableOp�
zero_padding2d/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d/Pad/paddings�
zero_padding2d/PadPadx$zero_padding2d/Pad/paddings:output:0*
T0*/
_output_shapes
:���������""2
zero_padding2d/Pad�
conv2d/Conv2D/ReadVariableOpReadVariableOp%conv2d_conv2d_readvariableop_resource*&
_output_shapes
:<*
dtype02
conv2d/Conv2D/ReadVariableOp�
conv2d/Conv2DConv2Dzero_padding2d/Pad:output:0$conv2d/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������  <*
paddingVALID*
strides
2
conv2d/Conv2Dt
conv2d/ReluReluconv2d/Conv2D:output:0*
T0*/
_output_shapes
:���������  <2
conv2d/Relu�
clamp/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
clamp/clip_by_value/Minimum/y�
clamp/clip_by_value/MinimumMinimumconv2d/Relu:activations:0&clamp/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������  <2
clamp/clip_by_value/Minimums
clamp/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp/clip_by_value/y�
clamp/clip_by_valueMaximumclamp/clip_by_value/Minimum:z:0clamp/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������  <2
clamp/clip_by_value�
IdentityIdentityclamp/clip_by_value:z:0^conv2d/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������  <2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  :2<
conv2d/Conv2D/ReadVariableOpconv2d/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������  

_user_specified_namex
�
t
3__inference_fused_conv2d_re_lu_layer_call_fn_182708
x
unknown
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallxunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������  <*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_1821772
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*/
_output_shapes
:���������  <2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  :22
StatefulPartitionedCallStatefulPartitionedCall:R N
/
_output_shapes
:���������  

_user_specified_namex
�
�
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182745
x+
'conv2d_1_conv2d_readvariableop_resource
identity��conv2d_1/Conv2D/ReadVariableOp�
max_pooling2d/MaxPoolMaxPoolx*/
_output_shapes
:���������<*
ksize
*
paddingVALID*
strides
2
max_pooling2d/MaxPool�
zero_padding2d_1/Pad/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             2
zero_padding2d_1/Pad/paddings�
zero_padding2d_1/PadPadmax_pooling2d/MaxPool:output:0&zero_padding2d_1/Pad/paddings:output:0*
T0*/
_output_shapes
:���������<2
zero_padding2d_1/Pad�
conv2d_1/Conv2D/ReadVariableOpReadVariableOp'conv2d_1_conv2d_readvariableop_resource*&
_output_shapes
:<<*
dtype02 
conv2d_1/Conv2D/ReadVariableOp�
conv2d_1/Conv2DConv2Dzero_padding2d_1/Pad:output:0&conv2d_1/Conv2D/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������<*
paddingVALID*
strides
2
conv2d_1/Conv2Dz
conv2d_1/ReluReluconv2d_1/Conv2D:output:0*
T0*/
_output_shapes
:���������<2
conv2d_1/Relu�
clamp_1/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2!
clamp_1/clip_by_value/Minimum/y�
clamp_1/clip_by_value/MinimumMinimumconv2d_1/Relu:activations:0(clamp_1/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������<2
clamp_1/clip_by_value/Minimumw
clamp_1/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ��2
clamp_1/clip_by_value/y�
clamp_1/clip_by_valueMaximum!clamp_1/clip_by_value/Minimum:z:0 clamp_1/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������<2
clamp_1/clip_by_value�
IdentityIdentityclamp_1/clip_by_value:z:0^conv2d_1/Conv2D/ReadVariableOp*
T0*/
_output_shapes
:���������<2

Identity"
identityIdentity:output:0*2
_input_shapes!
:���������  <:2@
conv2d_1/Conv2D/ReadVariableOpconv2d_1/Conv2D/ReadVariableOp:R N
/
_output_shapes
:���������  <

_user_specified_namex"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
C
input_18
serving_default_input_1:0���������  ?
fused_dense0
StatefulPartitionedCall:0���������
tensorflow/serving/predict:ʇ
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer_with_weights-2
layer-2
layer_with_weights-3
layer-3
layer-4
layer_with_weights-4
layer-5
	optimizer
	variables
	regularization_losses

trainable_variables
	keras_api

signatures
+�&call_and_return_all_conditional_losses
�_default_save_signature
�__call__"�
_tf_keras_sequential�{"class_name": "Sequential", "name": "sequential", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "config": {"name": "sequential", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 32, 32, 3]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_1"}}, {"class_name": "FusedConv2DReLU", "config": {"name": "FusedConv2DReLU", "filters": 60, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}, {"class_name": "FusedMaxPoolConv2DReLU", "config": {"name": "FusedMaxPoolConv2DReLU", "filters": 60, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}, {"class_name": "FusedMaxPoolConv2DReLU", "config": {"name": "FusedMaxPoolConv2DReLU", "filters": 56, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}, {"class_name": "FusedAvgPoolConv2DReLU", "config": {"name": "FusedAvgPoolConv2DReLU", "filters": 12, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}, {"class_name": "Flatten", "config": {"name": "flatten", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "FusedDense", "config": {"name": "FusedDense", "units": 10, "activation": null, "use_bias": false}}]}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 32, 32, 3]}, "is_graph_network": true, "keras_version": "2.4.0", "backend": "tensorflow", "model_config": {"class_name": "Sequential", "config": {"name": "sequential", "layers": [{"class_name": "InputLayer", "config": {"batch_input_shape": {"class_name": "__tuple__", "items": [null, 32, 32, 3]}, "dtype": "float32", "sparse": false, "ragged": false, "name": "input_1"}}, {"class_name": "FusedConv2DReLU", "config": {"name": "FusedConv2DReLU", "filters": 60, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}, {"class_name": "FusedMaxPoolConv2DReLU", "config": {"name": "FusedMaxPoolConv2DReLU", "filters": 60, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}, {"class_name": "FusedMaxPoolConv2DReLU", "config": {"name": "FusedMaxPoolConv2DReLU", "filters": 56, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}, {"class_name": "FusedAvgPoolConv2DReLU", "config": {"name": "FusedAvgPoolConv2DReLU", "filters": 12, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}, {"class_name": "Flatten", "config": {"name": "flatten", "trainable": true, "dtype": "float32", "data_format": "channels_last"}}, {"class_name": "FusedDense", "config": {"name": "FusedDense", "units": 10, "activation": null, "use_bias": false}}]}}, "training_config": {"loss": {"class_name": "SparseCategoricalCrossentropy", "config": {"reduction": "auto", "name": "sparse_categorical_crossentropy", "from_logits": true}}, "metrics": [[{"class_name": "MeanMetricWrapper", "config": {"name": "accuracy", "dtype": "float32", "fn": "sparse_categorical_accuracy"}}]], "weighted_metrics": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 4.999999873689376e-05, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
�
zeropadding

conv2d
quantize_pool

clamp_pool
quantize
	clamp
	variables
regularization_losses
trainable_variables
	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "FusedConv2DReLU", "name": "fused_conv2d_re_lu", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "FusedConv2DReLU", "filters": 60, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}
�
pool
zeropadding

conv2d
quantize_pool

clamp_pool
quantize
	clamp
	variables
regularization_losses
 trainable_variables
!	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "FusedMaxPoolConv2DReLU", "name": "fused_max_pool_conv2d_re_lu", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "FusedMaxPoolConv2DReLU", "filters": 60, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}
�
"pool
#zeropadding

$conv2d
%quantize_pool
&
clamp_pool
'quantize
	(clamp
)	variables
*regularization_losses
+trainable_variables
,	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "FusedMaxPoolConv2DReLU", "name": "fused_max_pool_conv2d_re_lu_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "FusedMaxPoolConv2DReLU", "filters": 56, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}
�
-pool
.zeropadding

/conv2d
0quantize_pool
1
clamp_pool
2quantize
	3clamp
4	variables
5regularization_losses
6trainable_variables
7	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "FusedAvgPoolConv2DReLU", "name": "fused_avg_pool_conv2d_re_lu", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "FusedAvgPoolConv2DReLU", "filters": 12, "kernel_size": 3, "strides": 1, "activation": "relu", "use_bias": false}}
�
8	variables
9regularization_losses
:trainable_variables
;	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Flatten", "name": "flatten", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "flatten", "trainable": true, "dtype": "float32", "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 1, "axes": {}}}}
�
	<Dense
=quantize
	>clamp
?	variables
@regularization_losses
Atrainable_variables
B	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "FusedDense", "name": "fused_dense", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "FusedDense", "units": 10, "activation": null, "use_bias": false}}
�

Cbeta_1

Dbeta_2
	Edecay
Flearning_rate
GiterHm�Im�Jm�Km�Lm�Hv�Iv�Jv�Kv�Lv�"
	optimizer
C
H0
I1
J2
K3
L4"
trackable_list_wrapper
 "
trackable_list_wrapper
C
H0
I1
J2
K3
L4"
trackable_list_wrapper
�

Mlayers
Nnon_trainable_variables
	variables
	regularization_losses
Ometrics
Player_regularization_losses
Qlayer_metrics

trainable_variables
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
�
R	variables
Sregularization_losses
Ttrainable_variables
U	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "ZeroPadding2D", "name": "zero_padding2d", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "zero_padding2d", "trainable": true, "dtype": "float32", "padding": {"class_name": "__tuple__", "items": [{"class_name": "__tuple__", "items": [1, 1]}, {"class_name": "__tuple__", "items": [1, 1]}]}, "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {}}}}
�	

Hkernel
V	variables
Wregularization_losses
Xtrainable_variables
Y	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Conv2D", "name": "conv2d", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "conv2d", "trainable": true, "dtype": "float32", "filters": 60, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "valid", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "relu", "use_bias": false, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 3}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 34, 34, 3]}}
�
Z	keras_api"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty", "trainable": true, "dtype": "float32"}}
�
[	keras_api"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_1", "trainable": true, "dtype": "float32"}}
�
\	keras_api"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty_2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_2", "trainable": true, "dtype": "float32"}}
�
]	variables
^regularization_losses
_trainable_variables
`	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Clamp", "name": "clamp", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "Clamp", "min_val": -1.0, "max_val": 1.0}}
'
H0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
H0"
trackable_list_wrapper
�
anon_trainable_variables
	variables
regularization_losses
blayer_metrics
cmetrics
dlayer_regularization_losses

elayers
trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
f	variables
gregularization_losses
htrainable_variables
i	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "MaxPooling2D", "name": "max_pooling2d", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "max_pooling2d", "trainable": true, "dtype": "float32", "pool_size": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "valid", "strides": {"class_name": "__tuple__", "items": [2, 2]}, "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {}}}}
�
j	variables
kregularization_losses
ltrainable_variables
m	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "ZeroPadding2D", "name": "zero_padding2d_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "zero_padding2d_1", "trainable": true, "dtype": "float32", "padding": {"class_name": "__tuple__", "items": [{"class_name": "__tuple__", "items": [1, 1]}, {"class_name": "__tuple__", "items": [1, 1]}]}, "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {}}}}
�	

Ikernel
n	variables
oregularization_losses
ptrainable_variables
q	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Conv2D", "name": "conv2d_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "conv2d_1", "trainable": true, "dtype": "float32", "filters": 60, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "valid", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "relu", "use_bias": false, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 60}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 18, 18, 60]}}
�
r	keras_api"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty_3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_3", "trainable": true, "dtype": "float32"}}
�
s	variables
tregularization_losses
utrainable_variables
v	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty_4", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_4", "trainable": true, "dtype": "float32"}}
�
w	keras_api"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty_5", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_5", "trainable": true, "dtype": "float32"}}
�
x	variables
yregularization_losses
ztrainable_variables
{	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Clamp", "name": "clamp_1", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "Clamp", "min_val": -1.0, "max_val": 1.0}}
'
I0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
I0"
trackable_list_wrapper
�
|non_trainable_variables
	variables
regularization_losses
}layer_metrics
~metrics
layer_regularization_losses
�layers
 trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "MaxPooling2D", "name": "max_pooling2d_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "max_pooling2d_1", "trainable": true, "dtype": "float32", "pool_size": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "valid", "strides": {"class_name": "__tuple__", "items": [2, 2]}, "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {}}}}
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "ZeroPadding2D", "name": "zero_padding2d_2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "zero_padding2d_2", "trainable": true, "dtype": "float32", "padding": {"class_name": "__tuple__", "items": [{"class_name": "__tuple__", "items": [1, 1]}, {"class_name": "__tuple__", "items": [1, 1]}]}, "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {}}}}
�	

Jkernel
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Conv2D", "name": "conv2d_2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "conv2d_2", "trainable": true, "dtype": "float32", "filters": 56, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "valid", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "relu", "use_bias": false, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 60}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 10, 10, 60]}}
�
�	keras_api"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty_6", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_6", "trainable": true, "dtype": "float32"}}
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty_7", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_7", "trainable": true, "dtype": "float32"}}
�
�	keras_api"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty_8", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_8", "trainable": true, "dtype": "float32"}}
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Clamp", "name": "clamp_2", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "Clamp", "min_val": -1.0, "max_val": 1.0}}
'
J0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
J0"
trackable_list_wrapper
�
�non_trainable_variables
)	variables
*regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
+trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "AveragePooling2D", "name": "average_pooling2d", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "average_pooling2d", "trainable": true, "dtype": "float32", "pool_size": {"class_name": "__tuple__", "items": [2, 2]}, "padding": "valid", "strides": {"class_name": "__tuple__", "items": [2, 2]}, "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {}}}}
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "ZeroPadding2D", "name": "zero_padding2d_3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "zero_padding2d_3", "trainable": true, "dtype": "float32", "padding": {"class_name": "__tuple__", "items": [{"class_name": "__tuple__", "items": [1, 1]}, {"class_name": "__tuple__", "items": [1, 1]}]}, "data_format": "channels_last"}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 4, "max_ndim": null, "min_ndim": null, "axes": {}}}}
�	

Kkernel
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Conv2D", "name": "conv2d_3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "conv2d_3", "trainable": true, "dtype": "float32", "filters": 12, "kernel_size": {"class_name": "__tuple__", "items": [3, 3]}, "strides": {"class_name": "__tuple__", "items": [1, 1]}, "padding": "valid", "data_format": "channels_last", "dilation_rate": {"class_name": "__tuple__", "items": [1, 1]}, "groups": 1, "activation": "relu", "use_bias": false, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 4, "axes": {"-1": 56}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 6, 6, 56]}}
�
�	keras_api"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty_9", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_9", "trainable": true, "dtype": "float32"}}
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Clamp", "name": "clamp_3", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "Clamp", "min_val": -1.0, "max_val": 1.0}}
�
�	keras_api"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty_10", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_10", "trainable": true, "dtype": "float32"}}
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Clamp", "name": "clamp_4", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "Clamp", "min_val": -1.0, "max_val": 1.0}}
'
K0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
K0"
trackable_list_wrapper
�
�non_trainable_variables
4	variables
5regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
6trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
8	variables
9regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
:trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�

Lkernel
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Dense", "name": "dense", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "dense", "trainable": true, "dtype": "float32", "units": 10, "activation": "linear", "use_bias": false, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 192}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 192]}}
�
�	keras_api"�
_tf_keras_layer�{"class_name": "Empty", "name": "empty_11", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "empty_11", "trainable": true, "dtype": "float32"}}
�
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"class_name": "Clamp", "name": "clamp_5", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "config": {"name": "Clamp", "min_val": -131072.0, "max_val": 131072.0}}
'
L0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
L0"
trackable_list_wrapper
�
�non_trainable_variables
?	variables
@regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
Atrainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
: (2beta_1
: (2beta_2
: (2decay
: (2learning_rate
:	 (2	Adam/iter
::8<2 fused_conv2d_re_lu/conv2d/kernel
E:C<<2+fused_max_pool_conv2d_re_lu/conv2d_1/kernel
G:E<82-fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel
E:C82+fused_avg_pool_conv2d_re_lu/conv2d_3/kernel
+:)	�
2fused_dense/dense/kernel
J
0
1
2
3
4
5"
trackable_list_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
R	variables
Sregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
Ttrainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
'
H0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
H0"
trackable_list_wrapper
�
�non_trainable_variables
V	variables
Wregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
Xtrainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
]	variables
^regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
_trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
J
0
1
2
3
4
5"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
f	variables
gregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
htrainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
j	variables
kregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
ltrainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
'
I0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
I0"
trackable_list_wrapper
�
�non_trainable_variables
n	variables
oregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
ptrainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
s	variables
tregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
utrainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
x	variables
yregularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
ztrainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
Q
0
1
2
3
4
5
6"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
'
J0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
J0"
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
Q
"0
#1
$2
%3
&4
'5
(6"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
'
K0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
K0"
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
Q
-0
.1
/2
03
14
25
36"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
'
L0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
L0"
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�	variables
�regularization_losses
�layer_metrics
�metrics
 �layer_regularization_losses
�layers
�trainable_variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
5
<0
=1
>2"
trackable_list_wrapper
�

�total

�count
�	variables
�	keras_api"�
_tf_keras_metricj{"class_name": "Mean", "name": "loss", "dtype": "float32", "config": {"name": "loss", "dtype": "float32"}}
�

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"�
_tf_keras_metric�{"class_name": "MeanMetricWrapper", "name": "accuracy", "dtype": "float32", "config": {"name": "accuracy", "dtype": "float32", "fn": "sparse_categorical_accuracy"}}
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
?:=<2'Adam/fused_conv2d_re_lu/conv2d/kernel/m
J:H<<22Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/m
L:J<824Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/m
J:H822Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/m
0:.	�
2Adam/fused_dense/dense/kernel/m
?:=<2'Adam/fused_conv2d_re_lu/conv2d/kernel/v
J:H<<22Adam/fused_max_pool_conv2d_re_lu/conv2d_1/kernel/v
L:J<824Adam/fused_max_pool_conv2d_re_lu_1/conv2d_2/kernel/v
J:H822Adam/fused_avg_pool_conv2d_re_lu/conv2d_3/kernel/v
0:.	�
2Adam/fused_dense/dense/kernel/v
�2�
F__inference_sequential_layer_call_and_return_conditional_losses_182407
F__inference_sequential_layer_call_and_return_conditional_losses_182427
F__inference_sequential_layer_call_and_return_conditional_losses_182643
F__inference_sequential_layer_call_and_return_conditional_losses_182583�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
!__inference__wrapped_model_182057�
���
FullArgSpec
args� 
varargsjargs
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *.�+
)�&
input_1���������  
�2�
+__inference_sequential_layer_call_fn_182658
+__inference_sequential_layer_call_fn_182673
+__inference_sequential_layer_call_fn_182498
+__inference_sequential_layer_call_fn_182463�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_182701
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_182687�
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
3__inference_fused_conv2d_re_lu_layer_call_fn_182708
3__inference_fused_conv2d_re_lu_layer_call_fn_182715�
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182745
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182730�
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
<__inference_fused_max_pool_conv2d_re_lu_layer_call_fn_182752
<__inference_fused_max_pool_conv2d_re_lu_layer_call_fn_182759�
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_182774
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_182789�
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
>__inference_fused_max_pool_conv2d_re_lu_1_layer_call_fn_182803
>__inference_fused_max_pool_conv2d_re_lu_1_layer_call_fn_182796�
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182841
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182822�
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
<__inference_fused_avg_pool_conv2d_re_lu_layer_call_fn_182848
<__inference_fused_avg_pool_conv2d_re_lu_layer_call_fn_182855�
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
C__inference_flatten_layer_call_and_return_conditional_losses_182861�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
(__inference_flatten_layer_call_fn_182866�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
G__inference_fused_dense_layer_call_and_return_conditional_losses_182888
G__inference_fused_dense_layer_call_and_return_conditional_losses_182877�
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
,__inference_fused_dense_layer_call_fn_182895
,__inference_fused_dense_layer_call_fn_182902�
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
$__inference_signature_wrapper_182523input_1"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
J__inference_zero_padding2d_layer_call_and_return_conditional_losses_182064�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2�
/__inference_zero_padding2d_layer_call_fn_182070�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
I__inference_max_pooling2d_layer_call_and_return_conditional_losses_182076�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2�
.__inference_max_pooling2d_layer_call_fn_182082�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2�
L__inference_zero_padding2d_1_layer_call_and_return_conditional_losses_182089�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2�
1__inference_zero_padding2d_1_layer_call_fn_182095�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
K__inference_max_pooling2d_1_layer_call_and_return_conditional_losses_182101�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2�
0__inference_max_pooling2d_1_layer_call_fn_182107�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2�
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_182114�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2�
1__inference_zero_padding2d_2_layer_call_fn_182120�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
M__inference_average_pooling2d_layer_call_and_return_conditional_losses_182126�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2�
2__inference_average_pooling2d_layer_call_fn_182132�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2�
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_182139�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2�
1__inference_zero_padding2d_3_layer_call_fn_182145�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *@�=
;�84������������������������������������
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2��
���
FullArgSpec$
args�
jself
jx

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 �
!__inference__wrapped_model_182057|HIJKL8�5
.�+
)�&
input_1���������  
� "9�6
4
fused_dense%�"
fused_dense���������
�
M__inference_average_pooling2d_layer_call_and_return_conditional_losses_182126�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
2__inference_average_pooling2d_layer_call_fn_182132�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
C__inference_flatten_layer_call_and_return_conditional_losses_182861a7�4
-�*
(�%
inputs���������
� "&�#
�
0����������
� �
(__inference_flatten_layer_call_fn_182866T7�4
-�*
(�%
inputs���������
� "������������
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182822jK6�3
,�)
#� 
x���������8
p
� "-�*
#� 
0���������
� �
W__inference_fused_avg_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182841jK6�3
,�)
#� 
x���������8
p 
� "-�*
#� 
0���������
� �
<__inference_fused_avg_pool_conv2d_re_lu_layer_call_fn_182848]K6�3
,�)
#� 
x���������8
p
� " �����������
<__inference_fused_avg_pool_conv2d_re_lu_layer_call_fn_182855]K6�3
,�)
#� 
x���������8
p 
� " �����������
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_182687jH6�3
,�)
#� 
x���������  
p
� "-�*
#� 
0���������  <
� �
N__inference_fused_conv2d_re_lu_layer_call_and_return_conditional_losses_182701jH6�3
,�)
#� 
x���������  
p 
� "-�*
#� 
0���������  <
� �
3__inference_fused_conv2d_re_lu_layer_call_fn_182708]H6�3
,�)
#� 
x���������  
p
� " ����������  <�
3__inference_fused_conv2d_re_lu_layer_call_fn_182715]H6�3
,�)
#� 
x���������  
p 
� " ����������  <�
G__inference_fused_dense_layer_call_and_return_conditional_losses_182877[L/�,
%�"
�
x����������
p
� "%�"
�
0���������

� �
G__inference_fused_dense_layer_call_and_return_conditional_losses_182888[L/�,
%�"
�
x����������
p 
� "%�"
�
0���������

� ~
,__inference_fused_dense_layer_call_fn_182895NL/�,
%�"
�
x����������
p
� "����������
~
,__inference_fused_dense_layer_call_fn_182902NL/�,
%�"
�
x����������
p 
� "����������
�
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_182774jJ6�3
,�)
#� 
x���������<
p
� "-�*
#� 
0���������8
� �
Y__inference_fused_max_pool_conv2d_re_lu_1_layer_call_and_return_conditional_losses_182789jJ6�3
,�)
#� 
x���������<
p 
� "-�*
#� 
0���������8
� �
>__inference_fused_max_pool_conv2d_re_lu_1_layer_call_fn_182796]J6�3
,�)
#� 
x���������<
p
� " ����������8�
>__inference_fused_max_pool_conv2d_re_lu_1_layer_call_fn_182803]J6�3
,�)
#� 
x���������<
p 
� " ����������8�
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182730jI6�3
,�)
#� 
x���������  <
p
� "-�*
#� 
0���������<
� �
W__inference_fused_max_pool_conv2d_re_lu_layer_call_and_return_conditional_losses_182745jI6�3
,�)
#� 
x���������  <
p 
� "-�*
#� 
0���������<
� �
<__inference_fused_max_pool_conv2d_re_lu_layer_call_fn_182752]I6�3
,�)
#� 
x���������  <
p
� " ����������<�
<__inference_fused_max_pool_conv2d_re_lu_layer_call_fn_182759]I6�3
,�)
#� 
x���������  <
p 
� " ����������<�
K__inference_max_pooling2d_1_layer_call_and_return_conditional_losses_182101�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
0__inference_max_pooling2d_1_layer_call_fn_182107�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
I__inference_max_pooling2d_layer_call_and_return_conditional_losses_182076�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
.__inference_max_pooling2d_layer_call_fn_182082�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
F__inference_sequential_layer_call_and_return_conditional_losses_182407pHIJKL@�=
6�3
)�&
input_1���������  
p

 
� "%�"
�
0���������

� �
F__inference_sequential_layer_call_and_return_conditional_losses_182427pHIJKL@�=
6�3
)�&
input_1���������  
p 

 
� "%�"
�
0���������

� �
F__inference_sequential_layer_call_and_return_conditional_losses_182583oHIJKL?�<
5�2
(�%
inputs���������  
p

 
� "%�"
�
0���������

� �
F__inference_sequential_layer_call_and_return_conditional_losses_182643oHIJKL?�<
5�2
(�%
inputs���������  
p 

 
� "%�"
�
0���������

� �
+__inference_sequential_layer_call_fn_182463cHIJKL@�=
6�3
)�&
input_1���������  
p

 
� "����������
�
+__inference_sequential_layer_call_fn_182498cHIJKL@�=
6�3
)�&
input_1���������  
p 

 
� "����������
�
+__inference_sequential_layer_call_fn_182658bHIJKL?�<
5�2
(�%
inputs���������  
p

 
� "����������
�
+__inference_sequential_layer_call_fn_182673bHIJKL?�<
5�2
(�%
inputs���������  
p 

 
� "����������
�
$__inference_signature_wrapper_182523�HIJKLC�@
� 
9�6
4
input_1)�&
input_1���������  "9�6
4
fused_dense%�"
fused_dense���������
�
L__inference_zero_padding2d_1_layer_call_and_return_conditional_losses_182089�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
1__inference_zero_padding2d_1_layer_call_fn_182095�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
L__inference_zero_padding2d_2_layer_call_and_return_conditional_losses_182114�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
1__inference_zero_padding2d_2_layer_call_fn_182120�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
L__inference_zero_padding2d_3_layer_call_and_return_conditional_losses_182139�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
1__inference_zero_padding2d_3_layer_call_fn_182145�R�O
H�E
C�@
inputs4������������������������������������
� ";�84�������������������������������������
J__inference_zero_padding2d_layer_call_and_return_conditional_losses_182064�R�O
H�E
C�@
inputs4������������������������������������
� "H�E
>�;
04������������������������������������
� �
/__inference_zero_padding2d_layer_call_fn_182070�R�O
H�E
C�@
inputs4������������������������������������
� ";�84������������������������������������