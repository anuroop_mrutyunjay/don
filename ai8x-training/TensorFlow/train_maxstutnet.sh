#!/bin/sh
./train.py --epochs 100 --batch_size 128 --optimizer Adam --lr 0.0003 --model maxstutnet_model --dataset maxstutnet --save-sample 46 --save-sample-per-class --channel-first --swap "$@"
./convert.py --saved-model export/maxstutnet --opset 10 --output export/maxstutnet/saved_model.onnx
