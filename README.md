# PAST : Privacy Aware Speech Therapist 
## Team Mates : Anuroop, Manu, Payal

### Executive Summary :
Over 70 million people across the world and 3 million Americans are affected by stuttering. Stuttering has deep interference to social and work life normalcy of an individual.

Currently most of the stuttering treatments revolve around a speech lab pathologist (SLP) who counts the number of times an individual stutters, judges the severity of the ailment, and prescribes the treatment. 

We want to automate the process of stutter detection. We propose a smart-device that can be trained to detect stutter and personalise itself based on the user's speech patterns. Our idealogy is to keep user's personal data as secure as possible so we conduct processing as close to the source of data as possible. 

Our design has dual objectives, 1. give accurate stutter-fluency feedback to our user, 2. Conduct inference on the edge device.

By automating stutter detection, we can make speech therapy accessible to a wider population. This will also reduce the subjective element that comes into picture when different SLPs manually detect stutter in a speech segment. Keeping the user's data local to the device will instill more confidence in using our technology, thus more effective therapy.

## Block Diagram
![Block Diagram](https://github.com/payalmohapatra/TinyML/blob/main/BD.PNG?raw=true)

## Implementation Details

### Data Curation 
We used data collected by _University College London's Archive of Stuttered Speech (UCLASS)_ for training our base model. 
For personalisation, we have followed the below protocol:
1. Read a monologue to log fluent class data. [(example dataset)](https://drive.google.com/file/d/140fE1Wnhh3tt7RjLu7iIv61LujorCrfh/view?usp=sharing)
2. Emulate repetition, prolongation and stammering of workd to log stutter class data. [(example dataset)](https://drive.google.com/file/d/1Mb5MQ1NOqRXFsnAFqHIuOmG2gURvzvVG/view?usp=sharing
) 

### Pre-processing
1. All audio data are clipped to 1 second.wav files.
2. Data parser to convert. .wav to .npz.

### Training
##### Training flow
Training a CNN model for deployment on the MAX78000FTHR Board requires the usage of a specific machine learning module called the AI8X-Training module. The AI8X-Training module can be used with either pytorch or Tensorflow as a backend. Tensorflow was used for this project. Initially, we trained our model (PASTNet) on a generic dataset (UCLASS).
###### PASTNet
We trained an Ensemble model consisting of two CNNs. The first was a 4 layer Conv1D network which we call the MFCCNet. The MFCCNet is fed the 1-second .wav files and converts them into Mel-frequency cepstrum coefficient spectograms of size 128x128. This MFCC spectogram is passed onto the second 5-layered Conv2D network to stratify as stutter or fluent.
![Ensemble Model](https://github.com/payalmohapatra/TinyML/blob/main/ensemble.PNG?raw=true)

The ai8x-training folder in this repo consists of code for training/re-training our model. Specific files and their functionality is explained below:

**ai8x-training (directory)**: 
1. **Tensorflow/datasets/maxstutnet.py**: Converts .wav files to .npz for training. Also responsible for train-test split. Can include data augmentation in the future. Basically, generates a usable dataset from raw audio files located under **Tensorflow/data/maxstutnet/raw/**
2. **Tensorflow/models/maxstutnet_model.py**: Defines model architecture. heavily inspired by CNN architecture described in research paper above (less the GRU, since max78k does not support it yet. Bummer!).
3. **Tensorflow/train_maxstutnet.sh**: Supplies cmd-line args necessary for ./train.py to train our model.
4. **Tensorflow/train.py**: Actual training file that takes model, dataset, epoch and various other parameters as cmd-line args and trains the model. Also saves sample input along with trained model and weights.
5. **Tensorflow/export/maxstutnet/**: Directory to find trained model along with sample input and onnx file. 

##### Personalisation of model
After training our model on generic UCLASS data, we then personalized our model to our user's speech profile using the ai8x flow.
1. Use the base model trained on UCLASS data.
2. Use data collected from each person to retrain this base model. 
3. Using a weighted loss function (to address the class imbalance of the dataset), obtain optimised weights for the model for that person using transfer learning.

### Synthesis
Models trained using the ai8x-training module cannot be directly deployed onto embedded boards (MAX78K) due to their innate size. Therefore, we perform the process of quantization to generate a minimised version of the model. During quantization, we ensure that there is no significant degradation of model performance through timely validation. Post quantization, we compile the model to low-level embedded-parsable code to deploy on the board. This process is called model synthesis and is performed using the ai8x-synthesis library.
For the ai8x-synthesis module to synthesize our model, we need to create a YAML config file which describes the architecture of our model. Specifically, the convolution operations used, the regularization parameters used as well as any pooling operations employed are mentioned in this file. The folder structure of this module as well as other important specifics are mentioned below:

**ai8x-synthesis (directory)**:
1. **networks/maxstutnet-hwc-tf.yaml**: YAML config file describing model architecture (CNN part only, need not include activation layer) as per ai8x-izer requirement mentioned [here](https://github.com/MaximIntegratedAI/ai8x-synthesis/blob/master/README.md#network-loader-ai8xize).
2. Use the quantized model saved under **ai8x-synthesis/tensoflow/tf-maxstutnet/** to integrate with main.c for inference which can be dumped on the board.
3. To regenerate quantized model, use command:
```./ai8xize.py --verbose -L --top-level cnn --test-dir tensorflow --prefix tf-maxstutnet --checkpoint-file ../ai8x-training/TensorFlow/export/maxstutnet/saved_model.onnx --config-file ./networks/maxstutnet-hwc-tf.yaml --sample-input ../ai8x-training/TensorFlow/export/maxstutnet/sampledata.npy --device MAX78000 --compact-data --mexpress --embedded-code --scale 1.0 --softmax --display-checkpoint --generate-dequantized-onnx-file --overwrite $@```

Once the synthesized model is generated, it can be dumped onto the board with minimal effort.

### Achievements
1. Making AI8X-Training and Synthesis environments OS-Agnostic 

2. Establishing a clear data pipeline for training and deploying models on the MAX78K

3. Integrating mic with V3 network architectures for real-time inference and enabling further research
### Post-Mortem/ Lessons Learnt
1. The MAX78k is optimised to have minimum data transfers between the CNN accelerator and Arm core. This means it cannot run any code, hence, we CANNOT TRAIN on it! It can be strictly used for inference purposes.
2. The ai8x-flow and Maxim SDK have limited documentation regarding its support on different OS. 
3. Does not support wireless communication protocols. 
4. Integration of mic was originally available only for V1 networks. We needed it to work for V3 network (to use MFCC coefficients as inputs to our network for stutter-fluency classification).
5. In general, documentation is scarce which makes working on a new application more challenging.
